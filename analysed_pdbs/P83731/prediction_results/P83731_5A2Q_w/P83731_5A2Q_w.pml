set grid_mode,1
set surface_quality, 0
load AARNA1123070W_w_BP.pdb
hide everything, AARNA1123070W_w_BP
show cartoon, AARNA1123070W_w_BP
color white, AARNA1123070W_w_BP
spectrum b, rainbow, AARNA1123070W_w_BP
set grid_slot, 1, AARNA1123070W_w_BP
center AARNA1123070W_w_BP
orient AARNA1123070W_w_BP
show surface, AARNA1123070W_w_BP
load AARNA1123070W_w_EC.pdb
hide everything, AARNA1123070W_w_EC
show cartoon, AARNA1123070W_w_EC
color white, AARNA1123070W_w_EC
spectrum b, rainbow, AARNA1123070W_w_EC
set grid_slot, 2, AARNA1123070W_w_EC
center AARNA1123070W_w_EC
orient AARNA1123070W_w_EC
show surface, AARNA1123070W_w_EC
load AARNA1123070W_w_LNMAX.pdb
hide everything, AARNA1123070W_w_LNMAX
show cartoon, AARNA1123070W_w_LNMAX
color white, AARNA1123070W_w_LNMAX
spectrum b, rainbow, AARNA1123070W_w_LNMAX
set grid_slot, 3, AARNA1123070W_w_LNMAX
center AARNA1123070W_w_LNMAX
orient AARNA1123070W_w_LNMAX
show surface, AARNA1123070W_w_LNMAX
load AARNA1123070W_w_LNMIN.pdb
hide everything, AARNA1123070W_w_LNMIN
show cartoon, AARNA1123070W_w_LNMIN
color white, AARNA1123070W_w_LNMIN
spectrum b, rainbow, AARNA1123070W_w_LNMIN
set grid_slot, 4, AARNA1123070W_w_LNMIN
center AARNA1123070W_w_LNMIN
orient AARNA1123070W_w_LNMIN
show surface, AARNA1123070W_w_LNMIN
set seq_view, 1
set transparency, 0.0
