set grid_mode,1
set surface_quality, 0
load AARNA961940W_A_BP.pdb
hide everything, AARNA961940W_A_BP
show cartoon, AARNA961940W_A_BP
color white, AARNA961940W_A_BP
spectrum b, rainbow, AARNA961940W_A_BP
set grid_slot, 1, AARNA961940W_A_BP
center AARNA961940W_A_BP
orient AARNA961940W_A_BP
show surface, AARNA961940W_A_BP
load AARNA961940W_A_EC.pdb
hide everything, AARNA961940W_A_EC
show cartoon, AARNA961940W_A_EC
color white, AARNA961940W_A_EC
spectrum b, rainbow, AARNA961940W_A_EC
set grid_slot, 2, AARNA961940W_A_EC
center AARNA961940W_A_EC
orient AARNA961940W_A_EC
show surface, AARNA961940W_A_EC
load AARNA961940W_A_LNMAX.pdb
hide everything, AARNA961940W_A_LNMAX
show cartoon, AARNA961940W_A_LNMAX
color white, AARNA961940W_A_LNMAX
spectrum b, rainbow, AARNA961940W_A_LNMAX
set grid_slot, 3, AARNA961940W_A_LNMAX
center AARNA961940W_A_LNMAX
orient AARNA961940W_A_LNMAX
show surface, AARNA961940W_A_LNMAX
load AARNA961940W_A_LNMIN.pdb
hide everything, AARNA961940W_A_LNMIN
show cartoon, AARNA961940W_A_LNMIN
color white, AARNA961940W_A_LNMIN
spectrum b, rainbow, AARNA961940W_A_LNMIN
set grid_slot, 4, AARNA961940W_A_LNMIN
center AARNA961940W_A_LNMIN
orient AARNA961940W_A_LNMIN
show surface, AARNA961940W_A_LNMIN
set seq_view, 1
set transparency, 0.0
