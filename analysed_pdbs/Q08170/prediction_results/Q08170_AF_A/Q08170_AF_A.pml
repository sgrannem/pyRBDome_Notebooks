set grid_mode,1
set surface_quality, 0
load AARNA789406W_A_BP.pdb
hide everything, AARNA789406W_A_BP
show cartoon, AARNA789406W_A_BP
color white, AARNA789406W_A_BP
spectrum b, rainbow, AARNA789406W_A_BP
set grid_slot, 1, AARNA789406W_A_BP
center AARNA789406W_A_BP
orient AARNA789406W_A_BP
show surface, AARNA789406W_A_BP
load AARNA789406W_A_EC.pdb
hide everything, AARNA789406W_A_EC
show cartoon, AARNA789406W_A_EC
color white, AARNA789406W_A_EC
spectrum b, rainbow, AARNA789406W_A_EC
set grid_slot, 2, AARNA789406W_A_EC
center AARNA789406W_A_EC
orient AARNA789406W_A_EC
show surface, AARNA789406W_A_EC
load AARNA789406W_A_LNMAX.pdb
hide everything, AARNA789406W_A_LNMAX
show cartoon, AARNA789406W_A_LNMAX
color white, AARNA789406W_A_LNMAX
spectrum b, rainbow, AARNA789406W_A_LNMAX
set grid_slot, 3, AARNA789406W_A_LNMAX
center AARNA789406W_A_LNMAX
orient AARNA789406W_A_LNMAX
show surface, AARNA789406W_A_LNMAX
load AARNA789406W_A_LNMIN.pdb
hide everything, AARNA789406W_A_LNMIN
show cartoon, AARNA789406W_A_LNMIN
color white, AARNA789406W_A_LNMIN
spectrum b, rainbow, AARNA789406W_A_LNMIN
set grid_slot, 4, AARNA789406W_A_LNMIN
center AARNA789406W_A_LNMIN
orient AARNA789406W_A_LNMIN
show surface, AARNA789406W_A_LNMIN
set seq_view, 1
set transparency, 0.0
