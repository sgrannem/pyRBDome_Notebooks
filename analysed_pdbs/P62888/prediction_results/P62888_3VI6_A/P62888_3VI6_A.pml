set grid_mode,1
set surface_quality, 0
load AARNA826825W_A_BP.pdb
hide everything, AARNA826825W_A_BP
show cartoon, AARNA826825W_A_BP
color white, AARNA826825W_A_BP
spectrum b, rainbow, AARNA826825W_A_BP
set grid_slot, 1, AARNA826825W_A_BP
center AARNA826825W_A_BP
orient AARNA826825W_A_BP
show surface, AARNA826825W_A_BP
load AARNA826825W_A_EC.pdb
hide everything, AARNA826825W_A_EC
show cartoon, AARNA826825W_A_EC
color white, AARNA826825W_A_EC
spectrum b, rainbow, AARNA826825W_A_EC
set grid_slot, 2, AARNA826825W_A_EC
center AARNA826825W_A_EC
orient AARNA826825W_A_EC
show surface, AARNA826825W_A_EC
load AARNA826825W_A_LNMAX.pdb
hide everything, AARNA826825W_A_LNMAX
show cartoon, AARNA826825W_A_LNMAX
color white, AARNA826825W_A_LNMAX
spectrum b, rainbow, AARNA826825W_A_LNMAX
set grid_slot, 3, AARNA826825W_A_LNMAX
center AARNA826825W_A_LNMAX
orient AARNA826825W_A_LNMAX
show surface, AARNA826825W_A_LNMAX
load AARNA826825W_A_LNMIN.pdb
hide everything, AARNA826825W_A_LNMIN
show cartoon, AARNA826825W_A_LNMIN
color white, AARNA826825W_A_LNMIN
spectrum b, rainbow, AARNA826825W_A_LNMIN
set grid_slot, 4, AARNA826825W_A_LNMIN
center AARNA826825W_A_LNMIN
orient AARNA826825W_A_LNMIN
show surface, AARNA826825W_A_LNMIN
set seq_view, 1
set transparency, 0.0
