set grid_mode,1
set surface_quality, 0
load AARNA3375721W_u_BP.pdb
hide everything, AARNA3375721W_u_BP
show cartoon, AARNA3375721W_u_BP
color white, AARNA3375721W_u_BP
spectrum b, rainbow, AARNA3375721W_u_BP
set grid_slot, 1, AARNA3375721W_u_BP
center AARNA3375721W_u_BP
orient AARNA3375721W_u_BP
show surface, AARNA3375721W_u_BP
load AARNA3375721W_u_EC.pdb
hide everything, AARNA3375721W_u_EC
show cartoon, AARNA3375721W_u_EC
color white, AARNA3375721W_u_EC
spectrum b, rainbow, AARNA3375721W_u_EC
set grid_slot, 2, AARNA3375721W_u_EC
center AARNA3375721W_u_EC
orient AARNA3375721W_u_EC
show surface, AARNA3375721W_u_EC
load AARNA3375721W_u_LNMAX.pdb
hide everything, AARNA3375721W_u_LNMAX
show cartoon, AARNA3375721W_u_LNMAX
color white, AARNA3375721W_u_LNMAX
spectrum b, rainbow, AARNA3375721W_u_LNMAX
set grid_slot, 3, AARNA3375721W_u_LNMAX
center AARNA3375721W_u_LNMAX
orient AARNA3375721W_u_LNMAX
show surface, AARNA3375721W_u_LNMAX
load AARNA3375721W_u_LNMIN.pdb
hide everything, AARNA3375721W_u_LNMIN
show cartoon, AARNA3375721W_u_LNMIN
color white, AARNA3375721W_u_LNMIN
spectrum b, rainbow, AARNA3375721W_u_LNMIN
set grid_slot, 4, AARNA3375721W_u_LNMIN
center AARNA3375721W_u_LNMIN
orient AARNA3375721W_u_LNMIN
show surface, AARNA3375721W_u_LNMIN
set seq_view, 1
set transparency, 0.0
