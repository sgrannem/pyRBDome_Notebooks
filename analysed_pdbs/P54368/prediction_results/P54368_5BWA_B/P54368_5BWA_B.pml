set grid_mode,1
set surface_quality, 0
load AARNA9367906W_B_BP.pdb
hide everything, AARNA9367906W_B_BP
show cartoon, AARNA9367906W_B_BP
color white, AARNA9367906W_B_BP
spectrum b, rainbow, AARNA9367906W_B_BP
set grid_slot, 1, AARNA9367906W_B_BP
center AARNA9367906W_B_BP
orient AARNA9367906W_B_BP
show surface, AARNA9367906W_B_BP
load AARNA9367906W_B_EC.pdb
hide everything, AARNA9367906W_B_EC
show cartoon, AARNA9367906W_B_EC
color white, AARNA9367906W_B_EC
spectrum b, rainbow, AARNA9367906W_B_EC
set grid_slot, 2, AARNA9367906W_B_EC
center AARNA9367906W_B_EC
orient AARNA9367906W_B_EC
show surface, AARNA9367906W_B_EC
load AARNA9367906W_B_LNMAX.pdb
hide everything, AARNA9367906W_B_LNMAX
show cartoon, AARNA9367906W_B_LNMAX
color white, AARNA9367906W_B_LNMAX
spectrum b, rainbow, AARNA9367906W_B_LNMAX
set grid_slot, 3, AARNA9367906W_B_LNMAX
center AARNA9367906W_B_LNMAX
orient AARNA9367906W_B_LNMAX
show surface, AARNA9367906W_B_LNMAX
load AARNA9367906W_B_LNMIN.pdb
hide everything, AARNA9367906W_B_LNMIN
show cartoon, AARNA9367906W_B_LNMIN
color white, AARNA9367906W_B_LNMIN
spectrum b, rainbow, AARNA9367906W_B_LNMIN
set grid_slot, 4, AARNA9367906W_B_LNMIN
center AARNA9367906W_B_LNMIN
orient AARNA9367906W_B_LNMIN
show surface, AARNA9367906W_B_LNMIN
set seq_view, 1
set transparency, 0.0
