set grid_mode,1
set surface_quality, 0
load AARNA7796961W_V_BP.pdb
hide everything, AARNA7796961W_V_BP
show cartoon, AARNA7796961W_V_BP
color white, AARNA7796961W_V_BP
spectrum b, rainbow, AARNA7796961W_V_BP
set grid_slot, 1, AARNA7796961W_V_BP
center AARNA7796961W_V_BP
orient AARNA7796961W_V_BP
show surface, AARNA7796961W_V_BP
load AARNA7796961W_V_EC.pdb
hide everything, AARNA7796961W_V_EC
show cartoon, AARNA7796961W_V_EC
color white, AARNA7796961W_V_EC
spectrum b, rainbow, AARNA7796961W_V_EC
set grid_slot, 2, AARNA7796961W_V_EC
center AARNA7796961W_V_EC
orient AARNA7796961W_V_EC
show surface, AARNA7796961W_V_EC
load AARNA7796961W_V_LNMAX.pdb
hide everything, AARNA7796961W_V_LNMAX
show cartoon, AARNA7796961W_V_LNMAX
color white, AARNA7796961W_V_LNMAX
spectrum b, rainbow, AARNA7796961W_V_LNMAX
set grid_slot, 3, AARNA7796961W_V_LNMAX
center AARNA7796961W_V_LNMAX
orient AARNA7796961W_V_LNMAX
show surface, AARNA7796961W_V_LNMAX
load AARNA7796961W_V_LNMIN.pdb
hide everything, AARNA7796961W_V_LNMIN
show cartoon, AARNA7796961W_V_LNMIN
color white, AARNA7796961W_V_LNMIN
spectrum b, rainbow, AARNA7796961W_V_LNMIN
set grid_slot, 4, AARNA7796961W_V_LNMIN
center AARNA7796961W_V_LNMIN
orient AARNA7796961W_V_LNMIN
show surface, AARNA7796961W_V_LNMIN
set seq_view, 1
set transparency, 0.0
