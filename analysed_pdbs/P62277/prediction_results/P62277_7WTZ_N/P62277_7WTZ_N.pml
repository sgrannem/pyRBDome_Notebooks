set grid_mode,1
set surface_quality, 0
load AARNA5298487W_N_BP.pdb
hide everything, AARNA5298487W_N_BP
show cartoon, AARNA5298487W_N_BP
color white, AARNA5298487W_N_BP
spectrum b, rainbow, AARNA5298487W_N_BP
set grid_slot, 1, AARNA5298487W_N_BP
center AARNA5298487W_N_BP
orient AARNA5298487W_N_BP
show surface, AARNA5298487W_N_BP
load AARNA5298487W_N_EC.pdb
hide everything, AARNA5298487W_N_EC
show cartoon, AARNA5298487W_N_EC
color white, AARNA5298487W_N_EC
spectrum b, rainbow, AARNA5298487W_N_EC
set grid_slot, 2, AARNA5298487W_N_EC
center AARNA5298487W_N_EC
orient AARNA5298487W_N_EC
show surface, AARNA5298487W_N_EC
load AARNA5298487W_N_LNMAX.pdb
hide everything, AARNA5298487W_N_LNMAX
show cartoon, AARNA5298487W_N_LNMAX
color white, AARNA5298487W_N_LNMAX
spectrum b, rainbow, AARNA5298487W_N_LNMAX
set grid_slot, 3, AARNA5298487W_N_LNMAX
center AARNA5298487W_N_LNMAX
orient AARNA5298487W_N_LNMAX
show surface, AARNA5298487W_N_LNMAX
load AARNA5298487W_N_LNMIN.pdb
hide everything, AARNA5298487W_N_LNMIN
show cartoon, AARNA5298487W_N_LNMIN
color white, AARNA5298487W_N_LNMIN
spectrum b, rainbow, AARNA5298487W_N_LNMIN
set grid_slot, 4, AARNA5298487W_N_LNMIN
center AARNA5298487W_N_LNMIN
orient AARNA5298487W_N_LNMIN
show surface, AARNA5298487W_N_LNMIN
set seq_view, 1
set transparency, 0.0
