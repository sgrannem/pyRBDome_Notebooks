set grid_mode,1
set surface_quality, 0
load AARNA8375555W_C_BP.pdb
hide everything, AARNA8375555W_C_BP
show cartoon, AARNA8375555W_C_BP
color white, AARNA8375555W_C_BP
spectrum b, rainbow, AARNA8375555W_C_BP
set grid_slot, 1, AARNA8375555W_C_BP
center AARNA8375555W_C_BP
orient AARNA8375555W_C_BP
show surface, AARNA8375555W_C_BP
load AARNA8375555W_C_EC.pdb
hide everything, AARNA8375555W_C_EC
show cartoon, AARNA8375555W_C_EC
color white, AARNA8375555W_C_EC
spectrum b, rainbow, AARNA8375555W_C_EC
set grid_slot, 2, AARNA8375555W_C_EC
center AARNA8375555W_C_EC
orient AARNA8375555W_C_EC
show surface, AARNA8375555W_C_EC
load AARNA8375555W_C_LNMAX.pdb
hide everything, AARNA8375555W_C_LNMAX
show cartoon, AARNA8375555W_C_LNMAX
color white, AARNA8375555W_C_LNMAX
spectrum b, rainbow, AARNA8375555W_C_LNMAX
set grid_slot, 3, AARNA8375555W_C_LNMAX
center AARNA8375555W_C_LNMAX
orient AARNA8375555W_C_LNMAX
show surface, AARNA8375555W_C_LNMAX
load AARNA8375555W_C_LNMIN.pdb
hide everything, AARNA8375555W_C_LNMIN
show cartoon, AARNA8375555W_C_LNMIN
color white, AARNA8375555W_C_LNMIN
spectrum b, rainbow, AARNA8375555W_C_LNMIN
set grid_slot, 4, AARNA8375555W_C_LNMIN
center AARNA8375555W_C_LNMIN
orient AARNA8375555W_C_LNMIN
show surface, AARNA8375555W_C_LNMIN
set seq_view, 1
set transparency, 0.0
