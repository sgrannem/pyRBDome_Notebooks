set grid_mode,1
set surface_quality, 0
load AARNA8584285W_R_BP.pdb
hide everything, AARNA8584285W_R_BP
show cartoon, AARNA8584285W_R_BP
color white, AARNA8584285W_R_BP
spectrum b, rainbow, AARNA8584285W_R_BP
set grid_slot, 1, AARNA8584285W_R_BP
center AARNA8584285W_R_BP
orient AARNA8584285W_R_BP
show surface, AARNA8584285W_R_BP
load AARNA8584285W_R_EC.pdb
hide everything, AARNA8584285W_R_EC
show cartoon, AARNA8584285W_R_EC
color white, AARNA8584285W_R_EC
spectrum b, rainbow, AARNA8584285W_R_EC
set grid_slot, 2, AARNA8584285W_R_EC
center AARNA8584285W_R_EC
orient AARNA8584285W_R_EC
show surface, AARNA8584285W_R_EC
load AARNA8584285W_R_LNMAX.pdb
hide everything, AARNA8584285W_R_LNMAX
show cartoon, AARNA8584285W_R_LNMAX
color white, AARNA8584285W_R_LNMAX
spectrum b, rainbow, AARNA8584285W_R_LNMAX
set grid_slot, 3, AARNA8584285W_R_LNMAX
center AARNA8584285W_R_LNMAX
orient AARNA8584285W_R_LNMAX
show surface, AARNA8584285W_R_LNMAX
load AARNA8584285W_R_LNMIN.pdb
hide everything, AARNA8584285W_R_LNMIN
show cartoon, AARNA8584285W_R_LNMIN
color white, AARNA8584285W_R_LNMIN
spectrum b, rainbow, AARNA8584285W_R_LNMIN
set grid_slot, 4, AARNA8584285W_R_LNMIN
center AARNA8584285W_R_LNMIN
orient AARNA8584285W_R_LNMIN
show surface, AARNA8584285W_R_LNMIN
set seq_view, 1
set transparency, 0.0
