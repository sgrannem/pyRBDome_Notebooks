set grid_mode,1
set surface_quality, 0
load AARNA2536180W_X_BP.pdb
hide everything, AARNA2536180W_X_BP
show cartoon, AARNA2536180W_X_BP
color white, AARNA2536180W_X_BP
spectrum b, rainbow, AARNA2536180W_X_BP
set grid_slot, 1, AARNA2536180W_X_BP
center AARNA2536180W_X_BP
orient AARNA2536180W_X_BP
show surface, AARNA2536180W_X_BP
load AARNA2536180W_X_EC.pdb
hide everything, AARNA2536180W_X_EC
show cartoon, AARNA2536180W_X_EC
color white, AARNA2536180W_X_EC
spectrum b, rainbow, AARNA2536180W_X_EC
set grid_slot, 2, AARNA2536180W_X_EC
center AARNA2536180W_X_EC
orient AARNA2536180W_X_EC
show surface, AARNA2536180W_X_EC
load AARNA2536180W_X_LNMAX.pdb
hide everything, AARNA2536180W_X_LNMAX
show cartoon, AARNA2536180W_X_LNMAX
color white, AARNA2536180W_X_LNMAX
spectrum b, rainbow, AARNA2536180W_X_LNMAX
set grid_slot, 3, AARNA2536180W_X_LNMAX
center AARNA2536180W_X_LNMAX
orient AARNA2536180W_X_LNMAX
show surface, AARNA2536180W_X_LNMAX
load AARNA2536180W_X_LNMIN.pdb
hide everything, AARNA2536180W_X_LNMIN
show cartoon, AARNA2536180W_X_LNMIN
color white, AARNA2536180W_X_LNMIN
spectrum b, rainbow, AARNA2536180W_X_LNMIN
set grid_slot, 4, AARNA2536180W_X_LNMIN
center AARNA2536180W_X_LNMIN
orient AARNA2536180W_X_LNMIN
show surface, AARNA2536180W_X_LNMIN
set seq_view, 1
set transparency, 0.0
