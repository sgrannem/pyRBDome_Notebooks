set grid_mode,1
set surface_quality, 0
load AARNA1309252W_C_BP.pdb
hide everything, AARNA1309252W_C_BP
show cartoon, AARNA1309252W_C_BP
color white, AARNA1309252W_C_BP
spectrum b, rainbow, AARNA1309252W_C_BP
set grid_slot, 1, AARNA1309252W_C_BP
center AARNA1309252W_C_BP
orient AARNA1309252W_C_BP
show surface, AARNA1309252W_C_BP
load AARNA1309252W_C_EC.pdb
hide everything, AARNA1309252W_C_EC
show cartoon, AARNA1309252W_C_EC
color white, AARNA1309252W_C_EC
spectrum b, rainbow, AARNA1309252W_C_EC
set grid_slot, 2, AARNA1309252W_C_EC
center AARNA1309252W_C_EC
orient AARNA1309252W_C_EC
show surface, AARNA1309252W_C_EC
load AARNA1309252W_C_LNMAX.pdb
hide everything, AARNA1309252W_C_LNMAX
show cartoon, AARNA1309252W_C_LNMAX
color white, AARNA1309252W_C_LNMAX
spectrum b, rainbow, AARNA1309252W_C_LNMAX
set grid_slot, 3, AARNA1309252W_C_LNMAX
center AARNA1309252W_C_LNMAX
orient AARNA1309252W_C_LNMAX
show surface, AARNA1309252W_C_LNMAX
load AARNA1309252W_C_LNMIN.pdb
hide everything, AARNA1309252W_C_LNMIN
show cartoon, AARNA1309252W_C_LNMIN
color white, AARNA1309252W_C_LNMIN
spectrum b, rainbow, AARNA1309252W_C_LNMIN
set grid_slot, 4, AARNA1309252W_C_LNMIN
center AARNA1309252W_C_LNMIN
orient AARNA1309252W_C_LNMIN
show surface, AARNA1309252W_C_LNMIN
set seq_view, 1
set transparency, 0.0
