set grid_mode,1
set surface_quality, 0
load AARNA185483W_Y_BP.pdb
hide everything, AARNA185483W_Y_BP
show cartoon, AARNA185483W_Y_BP
color white, AARNA185483W_Y_BP
spectrum b, rainbow, AARNA185483W_Y_BP
set grid_slot, 1, AARNA185483W_Y_BP
center AARNA185483W_Y_BP
orient AARNA185483W_Y_BP
show surface, AARNA185483W_Y_BP
load AARNA185483W_Y_EC.pdb
hide everything, AARNA185483W_Y_EC
show cartoon, AARNA185483W_Y_EC
color white, AARNA185483W_Y_EC
spectrum b, rainbow, AARNA185483W_Y_EC
set grid_slot, 2, AARNA185483W_Y_EC
center AARNA185483W_Y_EC
orient AARNA185483W_Y_EC
show surface, AARNA185483W_Y_EC
load AARNA185483W_Y_LNMAX.pdb
hide everything, AARNA185483W_Y_LNMAX
show cartoon, AARNA185483W_Y_LNMAX
color white, AARNA185483W_Y_LNMAX
spectrum b, rainbow, AARNA185483W_Y_LNMAX
set grid_slot, 3, AARNA185483W_Y_LNMAX
center AARNA185483W_Y_LNMAX
orient AARNA185483W_Y_LNMAX
show surface, AARNA185483W_Y_LNMAX
load AARNA185483W_Y_LNMIN.pdb
hide everything, AARNA185483W_Y_LNMIN
show cartoon, AARNA185483W_Y_LNMIN
color white, AARNA185483W_Y_LNMIN
spectrum b, rainbow, AARNA185483W_Y_LNMIN
set grid_slot, 4, AARNA185483W_Y_LNMIN
center AARNA185483W_Y_LNMIN
orient AARNA185483W_Y_LNMIN
show surface, AARNA185483W_Y_LNMIN
set seq_view, 1
set transparency, 0.0
