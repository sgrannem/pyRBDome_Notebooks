set surface_quality, 0
bg_color white
load Q8IWZ3_AF_A_BindUP.pdb,object=BindUP
hide everything, BindUP
show cartoon, BindUP
color white, BindUP
spectrum b, rainbow, BindUP
show surface, BindUP
load Q8IWZ3_AF_A_RNABindRPlus.pdb,object=RNABindRPlus
hide everything, RNABindRPlus
show cartoon, RNABindRPlus
color white, RNABindRPlus
spectrum b, rainbow, RNABindRPlus
show surface, RNABindRPlus
load Q8IWZ3_AF_A_DisoRDPbind.pdb,object=DisoRDPbind
hide everything, DisoRDPbind
show cartoon, DisoRDPbind
color white, DisoRDPbind
spectrum b, rainbow, DisoRDPbind
show surface, DisoRDPbind
load Q8IWZ3_AF_A_model_predictions.pdb,object=prediction
hide everything, prediction
show cartoon, prediction
color white, prediction
spectrum b, rainbow, prediction
show surface, prediction
load ../filtered_pdb_files/Q8IWZ3_AF.pdb, original
show surface, original
load ../filtered_pdb_files/Q8IWZ3_AF_A_peptides.pdb, object_cross_linked_peptides
load ../filtered_pdb_files/Q8IWZ3_AF_A_cross_linked_amino_acids.pdb, object_cross_linked_aa
load ../filtered_pdb_files/Q8IWZ3_AF_A_domains.pdb, object_domains
set seq_view, 1
align all