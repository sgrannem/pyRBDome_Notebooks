set grid_mode,1
set surface_quality, 0
load AARNA677073W_A_BP.pdb
hide everything, AARNA677073W_A_BP
show cartoon, AARNA677073W_A_BP
color white, AARNA677073W_A_BP
spectrum b, rainbow, AARNA677073W_A_BP
set grid_slot, 1, AARNA677073W_A_BP
center AARNA677073W_A_BP
orient AARNA677073W_A_BP
show surface, AARNA677073W_A_BP
load AARNA677073W_A_EC.pdb
hide everything, AARNA677073W_A_EC
show cartoon, AARNA677073W_A_EC
color white, AARNA677073W_A_EC
spectrum b, rainbow, AARNA677073W_A_EC
set grid_slot, 2, AARNA677073W_A_EC
center AARNA677073W_A_EC
orient AARNA677073W_A_EC
show surface, AARNA677073W_A_EC
load AARNA677073W_A_LNMAX.pdb
hide everything, AARNA677073W_A_LNMAX
show cartoon, AARNA677073W_A_LNMAX
color white, AARNA677073W_A_LNMAX
spectrum b, rainbow, AARNA677073W_A_LNMAX
set grid_slot, 3, AARNA677073W_A_LNMAX
center AARNA677073W_A_LNMAX
orient AARNA677073W_A_LNMAX
show surface, AARNA677073W_A_LNMAX
load AARNA677073W_A_LNMIN.pdb
hide everything, AARNA677073W_A_LNMIN
show cartoon, AARNA677073W_A_LNMIN
color white, AARNA677073W_A_LNMIN
spectrum b, rainbow, AARNA677073W_A_LNMIN
set grid_slot, 4, AARNA677073W_A_LNMIN
center AARNA677073W_A_LNMIN
orient AARNA677073W_A_LNMIN
show surface, AARNA677073W_A_LNMIN
set seq_view, 1
set transparency, 0.0
