set grid_mode,1
set surface_quality, 0
load AARNA6581295W_F_BP.pdb
hide everything, AARNA6581295W_F_BP
show cartoon, AARNA6581295W_F_BP
color white, AARNA6581295W_F_BP
spectrum b, rainbow, AARNA6581295W_F_BP
set grid_slot, 1, AARNA6581295W_F_BP
center AARNA6581295W_F_BP
orient AARNA6581295W_F_BP
show surface, AARNA6581295W_F_BP
load AARNA6581295W_F_EC.pdb
hide everything, AARNA6581295W_F_EC
show cartoon, AARNA6581295W_F_EC
color white, AARNA6581295W_F_EC
spectrum b, rainbow, AARNA6581295W_F_EC
set grid_slot, 2, AARNA6581295W_F_EC
center AARNA6581295W_F_EC
orient AARNA6581295W_F_EC
show surface, AARNA6581295W_F_EC
load AARNA6581295W_F_LNMAX.pdb
hide everything, AARNA6581295W_F_LNMAX
show cartoon, AARNA6581295W_F_LNMAX
color white, AARNA6581295W_F_LNMAX
spectrum b, rainbow, AARNA6581295W_F_LNMAX
set grid_slot, 3, AARNA6581295W_F_LNMAX
center AARNA6581295W_F_LNMAX
orient AARNA6581295W_F_LNMAX
show surface, AARNA6581295W_F_LNMAX
load AARNA6581295W_F_LNMIN.pdb
hide everything, AARNA6581295W_F_LNMIN
show cartoon, AARNA6581295W_F_LNMIN
color white, AARNA6581295W_F_LNMIN
spectrum b, rainbow, AARNA6581295W_F_LNMIN
set grid_slot, 4, AARNA6581295W_F_LNMIN
center AARNA6581295W_F_LNMIN
orient AARNA6581295W_F_LNMIN
show surface, AARNA6581295W_F_LNMIN
set seq_view, 1
set transparency, 0.0
