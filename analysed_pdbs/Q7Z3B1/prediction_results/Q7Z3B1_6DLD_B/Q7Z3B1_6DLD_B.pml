set grid_mode,1
set surface_quality, 0
load AARNA6784269W_B_BP.pdb
hide everything, AARNA6784269W_B_BP
show cartoon, AARNA6784269W_B_BP
color white, AARNA6784269W_B_BP
spectrum b, rainbow, AARNA6784269W_B_BP
set grid_slot, 1, AARNA6784269W_B_BP
center AARNA6784269W_B_BP
orient AARNA6784269W_B_BP
show surface, AARNA6784269W_B_BP
load AARNA6784269W_B_EC.pdb
hide everything, AARNA6784269W_B_EC
show cartoon, AARNA6784269W_B_EC
color white, AARNA6784269W_B_EC
spectrum b, rainbow, AARNA6784269W_B_EC
set grid_slot, 2, AARNA6784269W_B_EC
center AARNA6784269W_B_EC
orient AARNA6784269W_B_EC
show surface, AARNA6784269W_B_EC
load AARNA6784269W_B_LNMAX.pdb
hide everything, AARNA6784269W_B_LNMAX
show cartoon, AARNA6784269W_B_LNMAX
color white, AARNA6784269W_B_LNMAX
spectrum b, rainbow, AARNA6784269W_B_LNMAX
set grid_slot, 3, AARNA6784269W_B_LNMAX
center AARNA6784269W_B_LNMAX
orient AARNA6784269W_B_LNMAX
show surface, AARNA6784269W_B_LNMAX
load AARNA6784269W_B_LNMIN.pdb
hide everything, AARNA6784269W_B_LNMIN
show cartoon, AARNA6784269W_B_LNMIN
color white, AARNA6784269W_B_LNMIN
spectrum b, rainbow, AARNA6784269W_B_LNMIN
set grid_slot, 4, AARNA6784269W_B_LNMIN
center AARNA6784269W_B_LNMIN
orient AARNA6784269W_B_LNMIN
show surface, AARNA6784269W_B_LNMIN
set seq_view, 1
set transparency, 0.0
