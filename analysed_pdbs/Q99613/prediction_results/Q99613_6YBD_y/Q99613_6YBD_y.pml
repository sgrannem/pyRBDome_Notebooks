set grid_mode,1
set surface_quality, 0
load AARNA8155380W_y_BP.pdb
hide everything, AARNA8155380W_y_BP
show cartoon, AARNA8155380W_y_BP
color white, AARNA8155380W_y_BP
spectrum b, rainbow, AARNA8155380W_y_BP
set grid_slot, 1, AARNA8155380W_y_BP
center AARNA8155380W_y_BP
orient AARNA8155380W_y_BP
show surface, AARNA8155380W_y_BP
load AARNA8155380W_y_EC.pdb
hide everything, AARNA8155380W_y_EC
show cartoon, AARNA8155380W_y_EC
color white, AARNA8155380W_y_EC
spectrum b, rainbow, AARNA8155380W_y_EC
set grid_slot, 2, AARNA8155380W_y_EC
center AARNA8155380W_y_EC
orient AARNA8155380W_y_EC
show surface, AARNA8155380W_y_EC
load AARNA8155380W_y_LNMAX.pdb
hide everything, AARNA8155380W_y_LNMAX
show cartoon, AARNA8155380W_y_LNMAX
color white, AARNA8155380W_y_LNMAX
spectrum b, rainbow, AARNA8155380W_y_LNMAX
set grid_slot, 3, AARNA8155380W_y_LNMAX
center AARNA8155380W_y_LNMAX
orient AARNA8155380W_y_LNMAX
show surface, AARNA8155380W_y_LNMAX
load AARNA8155380W_y_LNMIN.pdb
hide everything, AARNA8155380W_y_LNMIN
show cartoon, AARNA8155380W_y_LNMIN
color white, AARNA8155380W_y_LNMIN
spectrum b, rainbow, AARNA8155380W_y_LNMIN
set grid_slot, 4, AARNA8155380W_y_LNMIN
center AARNA8155380W_y_LNMIN
orient AARNA8155380W_y_LNMIN
show surface, AARNA8155380W_y_LNMIN
set seq_view, 1
set transparency, 0.0
