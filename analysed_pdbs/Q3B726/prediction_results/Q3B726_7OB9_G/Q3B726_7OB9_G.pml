set grid_mode,1
set surface_quality, 0
load AARNA8120079W_G_BP.pdb
hide everything, AARNA8120079W_G_BP
show cartoon, AARNA8120079W_G_BP
color white, AARNA8120079W_G_BP
spectrum b, rainbow, AARNA8120079W_G_BP
set grid_slot, 1, AARNA8120079W_G_BP
center AARNA8120079W_G_BP
orient AARNA8120079W_G_BP
show surface, AARNA8120079W_G_BP
load AARNA8120079W_G_EC.pdb
hide everything, AARNA8120079W_G_EC
show cartoon, AARNA8120079W_G_EC
color white, AARNA8120079W_G_EC
spectrum b, rainbow, AARNA8120079W_G_EC
set grid_slot, 2, AARNA8120079W_G_EC
center AARNA8120079W_G_EC
orient AARNA8120079W_G_EC
show surface, AARNA8120079W_G_EC
load AARNA8120079W_G_LNMAX.pdb
hide everything, AARNA8120079W_G_LNMAX
show cartoon, AARNA8120079W_G_LNMAX
color white, AARNA8120079W_G_LNMAX
spectrum b, rainbow, AARNA8120079W_G_LNMAX
set grid_slot, 3, AARNA8120079W_G_LNMAX
center AARNA8120079W_G_LNMAX
orient AARNA8120079W_G_LNMAX
show surface, AARNA8120079W_G_LNMAX
load AARNA8120079W_G_LNMIN.pdb
hide everything, AARNA8120079W_G_LNMIN
show cartoon, AARNA8120079W_G_LNMIN
color white, AARNA8120079W_G_LNMIN
spectrum b, rainbow, AARNA8120079W_G_LNMIN
set grid_slot, 4, AARNA8120079W_G_LNMIN
center AARNA8120079W_G_LNMIN
orient AARNA8120079W_G_LNMIN
show surface, AARNA8120079W_G_LNMIN
set seq_view, 1
set transparency, 0.0
