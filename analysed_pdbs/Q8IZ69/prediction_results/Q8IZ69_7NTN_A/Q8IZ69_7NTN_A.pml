set grid_mode,1
set surface_quality, 0
load AARNA509805W_A_BP.pdb
hide everything, AARNA509805W_A_BP
show cartoon, AARNA509805W_A_BP
color white, AARNA509805W_A_BP
spectrum b, rainbow, AARNA509805W_A_BP
set grid_slot, 1, AARNA509805W_A_BP
center AARNA509805W_A_BP
orient AARNA509805W_A_BP
show surface, AARNA509805W_A_BP
load AARNA509805W_A_EC.pdb
hide everything, AARNA509805W_A_EC
show cartoon, AARNA509805W_A_EC
color white, AARNA509805W_A_EC
spectrum b, rainbow, AARNA509805W_A_EC
set grid_slot, 2, AARNA509805W_A_EC
center AARNA509805W_A_EC
orient AARNA509805W_A_EC
show surface, AARNA509805W_A_EC
load AARNA509805W_A_LNMAX.pdb
hide everything, AARNA509805W_A_LNMAX
show cartoon, AARNA509805W_A_LNMAX
color white, AARNA509805W_A_LNMAX
spectrum b, rainbow, AARNA509805W_A_LNMAX
set grid_slot, 3, AARNA509805W_A_LNMAX
center AARNA509805W_A_LNMAX
orient AARNA509805W_A_LNMAX
show surface, AARNA509805W_A_LNMAX
load AARNA509805W_A_LNMIN.pdb
hide everything, AARNA509805W_A_LNMIN
show cartoon, AARNA509805W_A_LNMIN
color white, AARNA509805W_A_LNMIN
spectrum b, rainbow, AARNA509805W_A_LNMIN
set grid_slot, 4, AARNA509805W_A_LNMIN
center AARNA509805W_A_LNMIN
orient AARNA509805W_A_LNMIN
show surface, AARNA509805W_A_LNMIN
set seq_view, 1
set transparency, 0.0
