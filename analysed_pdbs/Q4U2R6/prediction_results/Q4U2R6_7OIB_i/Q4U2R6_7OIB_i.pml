set grid_mode,1
set surface_quality, 0
load AARNA9253332W_i_BP.pdb
hide everything, AARNA9253332W_i_BP
show cartoon, AARNA9253332W_i_BP
color white, AARNA9253332W_i_BP
spectrum b, rainbow, AARNA9253332W_i_BP
set grid_slot, 1, AARNA9253332W_i_BP
center AARNA9253332W_i_BP
orient AARNA9253332W_i_BP
show surface, AARNA9253332W_i_BP
load AARNA9253332W_i_EC.pdb
hide everything, AARNA9253332W_i_EC
show cartoon, AARNA9253332W_i_EC
color white, AARNA9253332W_i_EC
spectrum b, rainbow, AARNA9253332W_i_EC
set grid_slot, 2, AARNA9253332W_i_EC
center AARNA9253332W_i_EC
orient AARNA9253332W_i_EC
show surface, AARNA9253332W_i_EC
load AARNA9253332W_i_LNMAX.pdb
hide everything, AARNA9253332W_i_LNMAX
show cartoon, AARNA9253332W_i_LNMAX
color white, AARNA9253332W_i_LNMAX
spectrum b, rainbow, AARNA9253332W_i_LNMAX
set grid_slot, 3, AARNA9253332W_i_LNMAX
center AARNA9253332W_i_LNMAX
orient AARNA9253332W_i_LNMAX
show surface, AARNA9253332W_i_LNMAX
load AARNA9253332W_i_LNMIN.pdb
hide everything, AARNA9253332W_i_LNMIN
show cartoon, AARNA9253332W_i_LNMIN
color white, AARNA9253332W_i_LNMIN
spectrum b, rainbow, AARNA9253332W_i_LNMIN
set grid_slot, 4, AARNA9253332W_i_LNMIN
center AARNA9253332W_i_LNMIN
orient AARNA9253332W_i_LNMIN
show surface, AARNA9253332W_i_LNMIN
set seq_view, 1
set transparency, 0.0
