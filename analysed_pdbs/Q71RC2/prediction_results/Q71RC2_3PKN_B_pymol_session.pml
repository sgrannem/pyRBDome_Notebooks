set surface_quality, 0
bg_color white
load Q71RC2_3PKN_B_PST_PRNA.pdb,object=PST_PRNA
hide everything, PST_PRNA
show cartoon, PST_PRNA
color white, PST_PRNA
spectrum b, rainbow, PST_PRNA
show surface, PST_PRNA
load Q71RC2_3PKN_B_FTMap_distances.pdb,object=FTMap
hide everything, FTMap
show cartoon, FTMap
color white, FTMap
spectrum b, rainbow_rev, FTMap
show surface, FTMap
load Q71RC2_3PKN_B_RNABindRPlus.pdb,object=RNABindRPlus
hide everything, RNABindRPlus
show cartoon, RNABindRPlus
color white, RNABindRPlus
spectrum b, rainbow, RNABindRPlus
show surface, RNABindRPlus
load Q71RC2_3PKN_B_DisoRDPbind.pdb,object=DisoRDPbind
hide everything, DisoRDPbind
show cartoon, DisoRDPbind
color white, DisoRDPbind
spectrum b, rainbow, DisoRDPbind
show surface, DisoRDPbind
load Q71RC2_3PKN_B_model_predictions.pdb,object=prediction
hide everything, prediction
show cartoon, prediction
color white, prediction
spectrum b, rainbow, prediction
show surface, prediction
load ../filtered_pdb_files/Q71RC2_3PKN.pdb, original
show surface, original
set seq_view, 1
align all