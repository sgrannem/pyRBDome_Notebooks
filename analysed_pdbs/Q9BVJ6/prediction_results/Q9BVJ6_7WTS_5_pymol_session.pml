set surface_quality, 0
bg_color white
load Q9BVJ6_7WTS_5_aaRNA.pdb,object=aaRNA
hide everything, aaRNA
show cartoon, aaRNA
color white, aaRNA
spectrum b, rainbow, aaRNA
show surface, aaRNA
load Q9BVJ6_7WTS_5_PST_PRNA.pdb,object=PST_PRNA
hide everything, PST_PRNA
show cartoon, PST_PRNA
color white, PST_PRNA
spectrum b, rainbow, PST_PRNA
show surface, PST_PRNA
load Q9BVJ6_7WTS_5_BindUP.pdb,object=BindUP
hide everything, BindUP
show cartoon, BindUP
color white, BindUP
spectrum b, rainbow, BindUP
show surface, BindUP
load Q9BVJ6_7WTS_5_RNABindRPlus.pdb,object=RNABindRPlus
hide everything, RNABindRPlus
show cartoon, RNABindRPlus
color white, RNABindRPlus
spectrum b, rainbow, RNABindRPlus
show surface, RNABindRPlus
load Q9BVJ6_7WTS_5_DisoRDPbind.pdb,object=DisoRDPbind
hide everything, DisoRDPbind
show cartoon, DisoRDPbind
color white, DisoRDPbind
spectrum b, rainbow, DisoRDPbind
show surface, DisoRDPbind
load Q9BVJ6_7WTS_5_model_predictions.pdb,object=prediction
hide everything, prediction
show cartoon, prediction
color white, prediction
spectrum b, rainbow, prediction
show surface, prediction
load ../filtered_pdb_files/Q9BVJ6_7WTS.pdb, original
show surface, original
load ../filtered_pdb_files/Q9BVJ6_7WTS_5_domains.pdb, object_domains
set seq_view, 1
align all