set grid_mode,1
set surface_quality, 0
load AARNA2831962W_1_BP.pdb
hide everything, AARNA2831962W_1_BP
show cartoon, AARNA2831962W_1_BP
color white, AARNA2831962W_1_BP
spectrum b, rainbow, AARNA2831962W_1_BP
set grid_slot, 1, AARNA2831962W_1_BP
center AARNA2831962W_1_BP
orient AARNA2831962W_1_BP
show surface, AARNA2831962W_1_BP
load AARNA2831962W_1_EC.pdb
hide everything, AARNA2831962W_1_EC
show cartoon, AARNA2831962W_1_EC
color white, AARNA2831962W_1_EC
spectrum b, rainbow, AARNA2831962W_1_EC
set grid_slot, 2, AARNA2831962W_1_EC
center AARNA2831962W_1_EC
orient AARNA2831962W_1_EC
show surface, AARNA2831962W_1_EC
load AARNA2831962W_1_LNMAX.pdb
hide everything, AARNA2831962W_1_LNMAX
show cartoon, AARNA2831962W_1_LNMAX
color white, AARNA2831962W_1_LNMAX
spectrum b, rainbow, AARNA2831962W_1_LNMAX
set grid_slot, 3, AARNA2831962W_1_LNMAX
center AARNA2831962W_1_LNMAX
orient AARNA2831962W_1_LNMAX
show surface, AARNA2831962W_1_LNMAX
load AARNA2831962W_1_LNMIN.pdb
hide everything, AARNA2831962W_1_LNMIN
show cartoon, AARNA2831962W_1_LNMIN
color white, AARNA2831962W_1_LNMIN
spectrum b, rainbow, AARNA2831962W_1_LNMIN
set grid_slot, 4, AARNA2831962W_1_LNMIN
center AARNA2831962W_1_LNMIN
orient AARNA2831962W_1_LNMIN
show surface, AARNA2831962W_1_LNMIN
set seq_view, 1
set transparency, 0.0
