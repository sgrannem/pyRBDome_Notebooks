set grid_mode,1
set surface_quality, 0
load AARNA6791798W_D_BP.pdb
hide everything, AARNA6791798W_D_BP
show cartoon, AARNA6791798W_D_BP
color white, AARNA6791798W_D_BP
spectrum b, rainbow, AARNA6791798W_D_BP
set grid_slot, 1, AARNA6791798W_D_BP
center AARNA6791798W_D_BP
orient AARNA6791798W_D_BP
show surface, AARNA6791798W_D_BP
load AARNA6791798W_D_EC.pdb
hide everything, AARNA6791798W_D_EC
show cartoon, AARNA6791798W_D_EC
color white, AARNA6791798W_D_EC
spectrum b, rainbow, AARNA6791798W_D_EC
set grid_slot, 2, AARNA6791798W_D_EC
center AARNA6791798W_D_EC
orient AARNA6791798W_D_EC
show surface, AARNA6791798W_D_EC
load AARNA6791798W_D_LNMAX.pdb
hide everything, AARNA6791798W_D_LNMAX
show cartoon, AARNA6791798W_D_LNMAX
color white, AARNA6791798W_D_LNMAX
spectrum b, rainbow, AARNA6791798W_D_LNMAX
set grid_slot, 3, AARNA6791798W_D_LNMAX
center AARNA6791798W_D_LNMAX
orient AARNA6791798W_D_LNMAX
show surface, AARNA6791798W_D_LNMAX
load AARNA6791798W_D_LNMIN.pdb
hide everything, AARNA6791798W_D_LNMIN
show cartoon, AARNA6791798W_D_LNMIN
color white, AARNA6791798W_D_LNMIN
spectrum b, rainbow, AARNA6791798W_D_LNMIN
set grid_slot, 4, AARNA6791798W_D_LNMIN
center AARNA6791798W_D_LNMIN
orient AARNA6791798W_D_LNMIN
show surface, AARNA6791798W_D_LNMIN
set seq_view, 1
set transparency, 0.0
