set grid_mode,1
set surface_quality, 0
load AARNA1651837W_E_BP.pdb
hide everything, AARNA1651837W_E_BP
show cartoon, AARNA1651837W_E_BP
color white, AARNA1651837W_E_BP
spectrum b, rainbow, AARNA1651837W_E_BP
set grid_slot, 1, AARNA1651837W_E_BP
center AARNA1651837W_E_BP
orient AARNA1651837W_E_BP
show surface, AARNA1651837W_E_BP
load AARNA1651837W_E_EC.pdb
hide everything, AARNA1651837W_E_EC
show cartoon, AARNA1651837W_E_EC
color white, AARNA1651837W_E_EC
spectrum b, rainbow, AARNA1651837W_E_EC
set grid_slot, 2, AARNA1651837W_E_EC
center AARNA1651837W_E_EC
orient AARNA1651837W_E_EC
show surface, AARNA1651837W_E_EC
load AARNA1651837W_E_LNMAX.pdb
hide everything, AARNA1651837W_E_LNMAX
show cartoon, AARNA1651837W_E_LNMAX
color white, AARNA1651837W_E_LNMAX
spectrum b, rainbow, AARNA1651837W_E_LNMAX
set grid_slot, 3, AARNA1651837W_E_LNMAX
center AARNA1651837W_E_LNMAX
orient AARNA1651837W_E_LNMAX
show surface, AARNA1651837W_E_LNMAX
load AARNA1651837W_E_LNMIN.pdb
hide everything, AARNA1651837W_E_LNMIN
show cartoon, AARNA1651837W_E_LNMIN
color white, AARNA1651837W_E_LNMIN
spectrum b, rainbow, AARNA1651837W_E_LNMIN
set grid_slot, 4, AARNA1651837W_E_LNMIN
center AARNA1651837W_E_LNMIN
orient AARNA1651837W_E_LNMIN
show surface, AARNA1651837W_E_LNMIN
set seq_view, 1
set transparency, 0.0
