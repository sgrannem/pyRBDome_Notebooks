set grid_mode,1
set surface_quality, 0
load AARNA3665007W_Z_BP.pdb
hide everything, AARNA3665007W_Z_BP
show cartoon, AARNA3665007W_Z_BP
color white, AARNA3665007W_Z_BP
spectrum b, rainbow, AARNA3665007W_Z_BP
set grid_slot, 1, AARNA3665007W_Z_BP
center AARNA3665007W_Z_BP
orient AARNA3665007W_Z_BP
show surface, AARNA3665007W_Z_BP
load AARNA3665007W_Z_EC.pdb
hide everything, AARNA3665007W_Z_EC
show cartoon, AARNA3665007W_Z_EC
color white, AARNA3665007W_Z_EC
spectrum b, rainbow, AARNA3665007W_Z_EC
set grid_slot, 2, AARNA3665007W_Z_EC
center AARNA3665007W_Z_EC
orient AARNA3665007W_Z_EC
show surface, AARNA3665007W_Z_EC
load AARNA3665007W_Z_LNMAX.pdb
hide everything, AARNA3665007W_Z_LNMAX
show cartoon, AARNA3665007W_Z_LNMAX
color white, AARNA3665007W_Z_LNMAX
spectrum b, rainbow, AARNA3665007W_Z_LNMAX
set grid_slot, 3, AARNA3665007W_Z_LNMAX
center AARNA3665007W_Z_LNMAX
orient AARNA3665007W_Z_LNMAX
show surface, AARNA3665007W_Z_LNMAX
load AARNA3665007W_Z_LNMIN.pdb
hide everything, AARNA3665007W_Z_LNMIN
show cartoon, AARNA3665007W_Z_LNMIN
color white, AARNA3665007W_Z_LNMIN
spectrum b, rainbow, AARNA3665007W_Z_LNMIN
set grid_slot, 4, AARNA3665007W_Z_LNMIN
center AARNA3665007W_Z_LNMIN
orient AARNA3665007W_Z_LNMIN
show surface, AARNA3665007W_Z_LNMIN
set seq_view, 1
set transparency, 0.0
