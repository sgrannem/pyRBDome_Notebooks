set surface_quality, 0
bg_color white
load P82675_8CSP_D_aaRNA.pdb,object=aaRNA
hide everything, aaRNA
show cartoon, aaRNA
color white, aaRNA
spectrum b, rainbow, aaRNA
show surface, aaRNA
load P82675_8CSP_D_PST_PRNA.pdb,object=PST_PRNA
hide everything, PST_PRNA
show cartoon, PST_PRNA
color white, PST_PRNA
spectrum b, rainbow, PST_PRNA
show surface, PST_PRNA
load P82675_8CSP_D_BindUP.pdb,object=BindUP
hide everything, BindUP
show cartoon, BindUP
color white, BindUP
spectrum b, rainbow, BindUP
show surface, BindUP
load P82675_8CSP_D_FTMap_distances.pdb,object=FTMap
hide everything, FTMap
show cartoon, FTMap
color white, FTMap
spectrum b, rainbow_rev, FTMap
show surface, FTMap
load P82675_8CSP_D_RNABindRPlus.pdb,object=RNABindRPlus
hide everything, RNABindRPlus
show cartoon, RNABindRPlus
color white, RNABindRPlus
spectrum b, rainbow, RNABindRPlus
show surface, RNABindRPlus
load P82675_8CSP_D_DisoRDPbind.pdb,object=DisoRDPbind
hide everything, DisoRDPbind
show cartoon, DisoRDPbind
color white, DisoRDPbind
spectrum b, rainbow, DisoRDPbind
show surface, DisoRDPbind
load P82675_8CSP_D_model_predictions.pdb,object=prediction
hide everything, prediction
show cartoon, prediction
color white, prediction
spectrum b, rainbow, prediction
show surface, prediction
load ../filtered_pdb_files/P82675_8CSP.pdb, original
show surface, original
load ../filtered_pdb_files/P82675_8CSP_D_domains.pdb, object_domains
set seq_view, 1
align all