set surface_quality, 0
bg_color white
load O60306_6ICZ_Q_aaRNA.pdb,object=aaRNA
hide everything, aaRNA
show cartoon, aaRNA
color white, aaRNA
spectrum b, rainbow, aaRNA
show surface, aaRNA
load O60306_6ICZ_Q_FTMap_distances.pdb,object=FTMap
hide everything, FTMap
show cartoon, FTMap
color white, FTMap
spectrum b, rainbow_rev, FTMap
show surface, FTMap
load O60306_6ICZ_Q_RNABindRPlus.pdb,object=RNABindRPlus
hide everything, RNABindRPlus
show cartoon, RNABindRPlus
color white, RNABindRPlus
spectrum b, rainbow, RNABindRPlus
show surface, RNABindRPlus
load O60306_6ICZ_Q_DisoRDPbind.pdb,object=DisoRDPbind
hide everything, DisoRDPbind
show cartoon, DisoRDPbind
color white, DisoRDPbind
spectrum b, rainbow, DisoRDPbind
show surface, DisoRDPbind
load O60306_6ICZ_Q_model_predictions.pdb,object=prediction
hide everything, prediction
show cartoon, prediction
color white, prediction
spectrum b, rainbow, prediction
show surface, prediction
load ../filtered_pdb_files/O60306_6ICZ.pdb, original
show surface, original
load ../filtered_pdb_files/O60306_6ICZ_Q_peptides.pdb, object_cross_linked_peptides
load ../filtered_pdb_files/O60306_6ICZ_Q_cross_linked_amino_acids.pdb, object_cross_linked_aa
load ../filtered_pdb_files/O60306_6ICZ_Q_domains.pdb, object_domains
set seq_view, 1
align all