set grid_mode,1
set surface_quality, 0
load AARNA8990101W_Q_BP.pdb
hide everything, AARNA8990101W_Q_BP
show cartoon, AARNA8990101W_Q_BP
color white, AARNA8990101W_Q_BP
spectrum b, rainbow, AARNA8990101W_Q_BP
set grid_slot, 1, AARNA8990101W_Q_BP
center AARNA8990101W_Q_BP
orient AARNA8990101W_Q_BP
show surface, AARNA8990101W_Q_BP
load AARNA8990101W_Q_EC.pdb
hide everything, AARNA8990101W_Q_EC
show cartoon, AARNA8990101W_Q_EC
color white, AARNA8990101W_Q_EC
spectrum b, rainbow, AARNA8990101W_Q_EC
set grid_slot, 2, AARNA8990101W_Q_EC
center AARNA8990101W_Q_EC
orient AARNA8990101W_Q_EC
show surface, AARNA8990101W_Q_EC
load AARNA8990101W_Q_LNMAX.pdb
hide everything, AARNA8990101W_Q_LNMAX
show cartoon, AARNA8990101W_Q_LNMAX
color white, AARNA8990101W_Q_LNMAX
spectrum b, rainbow, AARNA8990101W_Q_LNMAX
set grid_slot, 3, AARNA8990101W_Q_LNMAX
center AARNA8990101W_Q_LNMAX
orient AARNA8990101W_Q_LNMAX
show surface, AARNA8990101W_Q_LNMAX
load AARNA8990101W_Q_LNMIN.pdb
hide everything, AARNA8990101W_Q_LNMIN
show cartoon, AARNA8990101W_Q_LNMIN
color white, AARNA8990101W_Q_LNMIN
spectrum b, rainbow, AARNA8990101W_Q_LNMIN
set grid_slot, 4, AARNA8990101W_Q_LNMIN
center AARNA8990101W_Q_LNMIN
orient AARNA8990101W_Q_LNMIN
show surface, AARNA8990101W_Q_LNMIN
set seq_view, 1
set transparency, 0.0
