set surface_quality, 0
bg_color white
load O60832_7TRC_C_aaRNA.pdb,object=aaRNA
hide everything, aaRNA
show cartoon, aaRNA
color white, aaRNA
spectrum b, rainbow, aaRNA
show surface, aaRNA
load O60832_7TRC_C_PST_PRNA.pdb,object=PST_PRNA
hide everything, PST_PRNA
show cartoon, PST_PRNA
color white, PST_PRNA
spectrum b, rainbow, PST_PRNA
show surface, PST_PRNA
load O60832_7TRC_C_BindUP.pdb,object=BindUP
hide everything, BindUP
show cartoon, BindUP
color white, BindUP
spectrum b, rainbow, BindUP
show surface, BindUP
load O60832_7TRC_C_FTMap_distances.pdb,object=FTMap
hide everything, FTMap
show cartoon, FTMap
color white, FTMap
spectrum b, rainbow_rev, FTMap
show surface, FTMap
load O60832_7TRC_C_RNABindRPlus.pdb,object=RNABindRPlus
hide everything, RNABindRPlus
show cartoon, RNABindRPlus
color white, RNABindRPlus
spectrum b, rainbow, RNABindRPlus
show surface, RNABindRPlus
load O60832_7TRC_C_DisoRDPbind.pdb,object=DisoRDPbind
hide everything, DisoRDPbind
show cartoon, DisoRDPbind
color white, DisoRDPbind
spectrum b, rainbow, DisoRDPbind
show surface, DisoRDPbind
load O60832_7TRC_C_model_predictions.pdb,object=prediction
hide everything, prediction
show cartoon, prediction
color white, prediction
spectrum b, rainbow, prediction
show surface, prediction
load ../filtered_pdb_files/O60832_7TRC.pdb, original
show surface, original
load ../filtered_pdb_files/O60832_7TRC_C_peptides.pdb, object_cross_linked_peptides
load ../filtered_pdb_files/O60832_7TRC_C_cross_linked_amino_acids.pdb, object_cross_linked_aa
load ../filtered_pdb_files/O60832_7TRC_C_domains.pdb, object_domains
set seq_view, 1
align all