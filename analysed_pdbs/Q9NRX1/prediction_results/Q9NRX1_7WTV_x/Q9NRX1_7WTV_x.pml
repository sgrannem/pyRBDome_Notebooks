set grid_mode,1
set surface_quality, 0
load AARNA387514W_x_BP.pdb
hide everything, AARNA387514W_x_BP
show cartoon, AARNA387514W_x_BP
color white, AARNA387514W_x_BP
spectrum b, rainbow, AARNA387514W_x_BP
set grid_slot, 1, AARNA387514W_x_BP
center AARNA387514W_x_BP
orient AARNA387514W_x_BP
show surface, AARNA387514W_x_BP
load AARNA387514W_x_EC.pdb
hide everything, AARNA387514W_x_EC
show cartoon, AARNA387514W_x_EC
color white, AARNA387514W_x_EC
spectrum b, rainbow, AARNA387514W_x_EC
set grid_slot, 2, AARNA387514W_x_EC
center AARNA387514W_x_EC
orient AARNA387514W_x_EC
show surface, AARNA387514W_x_EC
load AARNA387514W_x_LNMAX.pdb
hide everything, AARNA387514W_x_LNMAX
show cartoon, AARNA387514W_x_LNMAX
color white, AARNA387514W_x_LNMAX
spectrum b, rainbow, AARNA387514W_x_LNMAX
set grid_slot, 3, AARNA387514W_x_LNMAX
center AARNA387514W_x_LNMAX
orient AARNA387514W_x_LNMAX
show surface, AARNA387514W_x_LNMAX
load AARNA387514W_x_LNMIN.pdb
hide everything, AARNA387514W_x_LNMIN
show cartoon, AARNA387514W_x_LNMIN
color white, AARNA387514W_x_LNMIN
spectrum b, rainbow, AARNA387514W_x_LNMIN
set grid_slot, 4, AARNA387514W_x_LNMIN
center AARNA387514W_x_LNMIN
orient AARNA387514W_x_LNMIN
show surface, AARNA387514W_x_LNMIN
set seq_view, 1
set transparency, 0.0
