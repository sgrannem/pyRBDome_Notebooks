set grid_mode,1
set surface_quality, 0
load AARNA364700W_o_BP.pdb
hide everything, AARNA364700W_o_BP
show cartoon, AARNA364700W_o_BP
color white, AARNA364700W_o_BP
spectrum b, rainbow, AARNA364700W_o_BP
set grid_slot, 1, AARNA364700W_o_BP
center AARNA364700W_o_BP
orient AARNA364700W_o_BP
show surface, AARNA364700W_o_BP
load AARNA364700W_o_EC.pdb
hide everything, AARNA364700W_o_EC
show cartoon, AARNA364700W_o_EC
color white, AARNA364700W_o_EC
spectrum b, rainbow, AARNA364700W_o_EC
set grid_slot, 2, AARNA364700W_o_EC
center AARNA364700W_o_EC
orient AARNA364700W_o_EC
show surface, AARNA364700W_o_EC
load AARNA364700W_o_LNMAX.pdb
hide everything, AARNA364700W_o_LNMAX
show cartoon, AARNA364700W_o_LNMAX
color white, AARNA364700W_o_LNMAX
spectrum b, rainbow, AARNA364700W_o_LNMAX
set grid_slot, 3, AARNA364700W_o_LNMAX
center AARNA364700W_o_LNMAX
orient AARNA364700W_o_LNMAX
show surface, AARNA364700W_o_LNMAX
load AARNA364700W_o_LNMIN.pdb
hide everything, AARNA364700W_o_LNMIN
show cartoon, AARNA364700W_o_LNMIN
color white, AARNA364700W_o_LNMIN
spectrum b, rainbow, AARNA364700W_o_LNMIN
set grid_slot, 4, AARNA364700W_o_LNMIN
center AARNA364700W_o_LNMIN
orient AARNA364700W_o_LNMIN
show surface, AARNA364700W_o_LNMIN
set seq_view, 1
set transparency, 0.0
