set grid_mode,1
set surface_quality, 0
load AARNA7511406W_c_BP.pdb
hide everything, AARNA7511406W_c_BP
show cartoon, AARNA7511406W_c_BP
color white, AARNA7511406W_c_BP
spectrum b, rainbow, AARNA7511406W_c_BP
set grid_slot, 1, AARNA7511406W_c_BP
center AARNA7511406W_c_BP
orient AARNA7511406W_c_BP
show surface, AARNA7511406W_c_BP
load AARNA7511406W_c_EC.pdb
hide everything, AARNA7511406W_c_EC
show cartoon, AARNA7511406W_c_EC
color white, AARNA7511406W_c_EC
spectrum b, rainbow, AARNA7511406W_c_EC
set grid_slot, 2, AARNA7511406W_c_EC
center AARNA7511406W_c_EC
orient AARNA7511406W_c_EC
show surface, AARNA7511406W_c_EC
load AARNA7511406W_c_LNMAX.pdb
hide everything, AARNA7511406W_c_LNMAX
show cartoon, AARNA7511406W_c_LNMAX
color white, AARNA7511406W_c_LNMAX
spectrum b, rainbow, AARNA7511406W_c_LNMAX
set grid_slot, 3, AARNA7511406W_c_LNMAX
center AARNA7511406W_c_LNMAX
orient AARNA7511406W_c_LNMAX
show surface, AARNA7511406W_c_LNMAX
load AARNA7511406W_c_LNMIN.pdb
hide everything, AARNA7511406W_c_LNMIN
show cartoon, AARNA7511406W_c_LNMIN
color white, AARNA7511406W_c_LNMIN
spectrum b, rainbow, AARNA7511406W_c_LNMIN
set grid_slot, 4, AARNA7511406W_c_LNMIN
center AARNA7511406W_c_LNMIN
orient AARNA7511406W_c_LNMIN
show surface, AARNA7511406W_c_LNMIN
set seq_view, 1
set transparency, 0.0
