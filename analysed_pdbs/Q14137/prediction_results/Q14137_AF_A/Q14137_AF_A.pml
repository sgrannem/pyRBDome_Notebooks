set grid_mode,1
set surface_quality, 0
load AARNA988025W_A_BP.pdb
hide everything, AARNA988025W_A_BP
show cartoon, AARNA988025W_A_BP
color white, AARNA988025W_A_BP
spectrum b, rainbow, AARNA988025W_A_BP
set grid_slot, 1, AARNA988025W_A_BP
center AARNA988025W_A_BP
orient AARNA988025W_A_BP
show surface, AARNA988025W_A_BP
load AARNA988025W_A_EC.pdb
hide everything, AARNA988025W_A_EC
show cartoon, AARNA988025W_A_EC
color white, AARNA988025W_A_EC
spectrum b, rainbow, AARNA988025W_A_EC
set grid_slot, 2, AARNA988025W_A_EC
center AARNA988025W_A_EC
orient AARNA988025W_A_EC
show surface, AARNA988025W_A_EC
load AARNA988025W_A_LNMAX.pdb
hide everything, AARNA988025W_A_LNMAX
show cartoon, AARNA988025W_A_LNMAX
color white, AARNA988025W_A_LNMAX
spectrum b, rainbow, AARNA988025W_A_LNMAX
set grid_slot, 3, AARNA988025W_A_LNMAX
center AARNA988025W_A_LNMAX
orient AARNA988025W_A_LNMAX
show surface, AARNA988025W_A_LNMAX
load AARNA988025W_A_LNMIN.pdb
hide everything, AARNA988025W_A_LNMIN
show cartoon, AARNA988025W_A_LNMIN
color white, AARNA988025W_A_LNMIN
spectrum b, rainbow, AARNA988025W_A_LNMIN
set grid_slot, 4, AARNA988025W_A_LNMIN
center AARNA988025W_A_LNMIN
orient AARNA988025W_A_LNMIN
show surface, AARNA988025W_A_LNMIN
set seq_view, 1
set transparency, 0.0
