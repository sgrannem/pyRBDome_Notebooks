set surface_quality, 0
bg_color white
load Q7KZ85_6TED_M_aaRNA.pdb,object=aaRNA
hide everything, aaRNA
show cartoon, aaRNA
color white, aaRNA
spectrum b, rainbow, aaRNA
show surface, aaRNA
load Q7KZ85_6TED_M_FTMap_distances.pdb,object=FTMap
hide everything, FTMap
show cartoon, FTMap
color white, FTMap
spectrum b, rainbow_rev, FTMap
show surface, FTMap
load Q7KZ85_6TED_M_RNABindRPlus.pdb,object=RNABindRPlus
hide everything, RNABindRPlus
show cartoon, RNABindRPlus
color white, RNABindRPlus
spectrum b, rainbow, RNABindRPlus
show surface, RNABindRPlus
load Q7KZ85_6TED_M_DisoRDPbind.pdb,object=DisoRDPbind
hide everything, DisoRDPbind
show cartoon, DisoRDPbind
color white, DisoRDPbind
spectrum b, rainbow, DisoRDPbind
show surface, DisoRDPbind
load Q7KZ85_6TED_M_model_predictions.pdb,object=prediction
hide everything, prediction
show cartoon, prediction
color white, prediction
spectrum b, rainbow, prediction
show surface, prediction
load ../filtered_pdb_files/Q7KZ85_6TED.pdb, original
show surface, original
load ../filtered_pdb_files/Q7KZ85_6TED_M_peptides.pdb, object_cross_linked_peptides
load ../filtered_pdb_files/Q7KZ85_6TED_M_cross_linked_amino_acids.pdb, object_cross_linked_aa
load ../filtered_pdb_files/Q7KZ85_6TED_M_domains.pdb, object_domains
set seq_view, 1
align all