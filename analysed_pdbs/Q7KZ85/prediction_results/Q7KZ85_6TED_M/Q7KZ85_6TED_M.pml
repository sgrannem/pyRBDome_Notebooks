set grid_mode,1
set surface_quality, 0
load AARNA670996W_M_BP.pdb
hide everything, AARNA670996W_M_BP
show cartoon, AARNA670996W_M_BP
color white, AARNA670996W_M_BP
spectrum b, rainbow, AARNA670996W_M_BP
set grid_slot, 1, AARNA670996W_M_BP
center AARNA670996W_M_BP
orient AARNA670996W_M_BP
show surface, AARNA670996W_M_BP
load AARNA670996W_M_EC.pdb
hide everything, AARNA670996W_M_EC
show cartoon, AARNA670996W_M_EC
color white, AARNA670996W_M_EC
spectrum b, rainbow, AARNA670996W_M_EC
set grid_slot, 2, AARNA670996W_M_EC
center AARNA670996W_M_EC
orient AARNA670996W_M_EC
show surface, AARNA670996W_M_EC
load AARNA670996W_M_LNMAX.pdb
hide everything, AARNA670996W_M_LNMAX
show cartoon, AARNA670996W_M_LNMAX
color white, AARNA670996W_M_LNMAX
spectrum b, rainbow, AARNA670996W_M_LNMAX
set grid_slot, 3, AARNA670996W_M_LNMAX
center AARNA670996W_M_LNMAX
orient AARNA670996W_M_LNMAX
show surface, AARNA670996W_M_LNMAX
load AARNA670996W_M_LNMIN.pdb
hide everything, AARNA670996W_M_LNMIN
show cartoon, AARNA670996W_M_LNMIN
color white, AARNA670996W_M_LNMIN
spectrum b, rainbow, AARNA670996W_M_LNMIN
set grid_slot, 4, AARNA670996W_M_LNMIN
center AARNA670996W_M_LNMIN
orient AARNA670996W_M_LNMIN
show surface, AARNA670996W_M_LNMIN
set seq_view, 1
set transparency, 0.0
