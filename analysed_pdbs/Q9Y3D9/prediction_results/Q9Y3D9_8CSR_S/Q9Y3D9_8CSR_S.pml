set grid_mode,1
set surface_quality, 0
load AARNA5183169W_S_BP.pdb
hide everything, AARNA5183169W_S_BP
show cartoon, AARNA5183169W_S_BP
color white, AARNA5183169W_S_BP
spectrum b, rainbow, AARNA5183169W_S_BP
set grid_slot, 1, AARNA5183169W_S_BP
center AARNA5183169W_S_BP
orient AARNA5183169W_S_BP
show surface, AARNA5183169W_S_BP
load AARNA5183169W_S_EC.pdb
hide everything, AARNA5183169W_S_EC
show cartoon, AARNA5183169W_S_EC
color white, AARNA5183169W_S_EC
spectrum b, rainbow, AARNA5183169W_S_EC
set grid_slot, 2, AARNA5183169W_S_EC
center AARNA5183169W_S_EC
orient AARNA5183169W_S_EC
show surface, AARNA5183169W_S_EC
load AARNA5183169W_S_LNMAX.pdb
hide everything, AARNA5183169W_S_LNMAX
show cartoon, AARNA5183169W_S_LNMAX
color white, AARNA5183169W_S_LNMAX
spectrum b, rainbow, AARNA5183169W_S_LNMAX
set grid_slot, 3, AARNA5183169W_S_LNMAX
center AARNA5183169W_S_LNMAX
orient AARNA5183169W_S_LNMAX
show surface, AARNA5183169W_S_LNMAX
load AARNA5183169W_S_LNMIN.pdb
hide everything, AARNA5183169W_S_LNMIN
show cartoon, AARNA5183169W_S_LNMIN
color white, AARNA5183169W_S_LNMIN
spectrum b, rainbow, AARNA5183169W_S_LNMIN
set grid_slot, 4, AARNA5183169W_S_LNMIN
center AARNA5183169W_S_LNMIN
orient AARNA5183169W_S_LNMIN
show surface, AARNA5183169W_S_LNMIN
set seq_view, 1
set transparency, 0.0
