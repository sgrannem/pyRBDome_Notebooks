set grid_mode,1
set surface_quality, 0
load AARNA364365W_x_BP.pdb
hide everything, AARNA364365W_x_BP
show cartoon, AARNA364365W_x_BP
color white, AARNA364365W_x_BP
spectrum b, rainbow, AARNA364365W_x_BP
set grid_slot, 1, AARNA364365W_x_BP
center AARNA364365W_x_BP
orient AARNA364365W_x_BP
show surface, AARNA364365W_x_BP
load AARNA364365W_x_EC.pdb
hide everything, AARNA364365W_x_EC
show cartoon, AARNA364365W_x_EC
color white, AARNA364365W_x_EC
spectrum b, rainbow, AARNA364365W_x_EC
set grid_slot, 2, AARNA364365W_x_EC
center AARNA364365W_x_EC
orient AARNA364365W_x_EC
show surface, AARNA364365W_x_EC
load AARNA364365W_x_LNMAX.pdb
hide everything, AARNA364365W_x_LNMAX
show cartoon, AARNA364365W_x_LNMAX
color white, AARNA364365W_x_LNMAX
spectrum b, rainbow, AARNA364365W_x_LNMAX
set grid_slot, 3, AARNA364365W_x_LNMAX
center AARNA364365W_x_LNMAX
orient AARNA364365W_x_LNMAX
show surface, AARNA364365W_x_LNMAX
load AARNA364365W_x_LNMIN.pdb
hide everything, AARNA364365W_x_LNMIN
show cartoon, AARNA364365W_x_LNMIN
color white, AARNA364365W_x_LNMIN
spectrum b, rainbow, AARNA364365W_x_LNMIN
set grid_slot, 4, AARNA364365W_x_LNMIN
center AARNA364365W_x_LNMIN
orient AARNA364365W_x_LNMIN
show surface, AARNA364365W_x_LNMIN
set seq_view, 1
set transparency, 0.0
