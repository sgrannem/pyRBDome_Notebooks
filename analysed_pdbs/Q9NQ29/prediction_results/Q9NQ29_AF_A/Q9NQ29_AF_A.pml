set grid_mode,1
set surface_quality, 0
load AARNA75352W_A_BP.pdb
hide everything, AARNA75352W_A_BP
show cartoon, AARNA75352W_A_BP
color white, AARNA75352W_A_BP
spectrum b, rainbow, AARNA75352W_A_BP
set grid_slot, 1, AARNA75352W_A_BP
center AARNA75352W_A_BP
orient AARNA75352W_A_BP
show surface, AARNA75352W_A_BP
load AARNA75352W_A_EC.pdb
hide everything, AARNA75352W_A_EC
show cartoon, AARNA75352W_A_EC
color white, AARNA75352W_A_EC
spectrum b, rainbow, AARNA75352W_A_EC
set grid_slot, 2, AARNA75352W_A_EC
center AARNA75352W_A_EC
orient AARNA75352W_A_EC
show surface, AARNA75352W_A_EC
load AARNA75352W_A_LNMAX.pdb
hide everything, AARNA75352W_A_LNMAX
show cartoon, AARNA75352W_A_LNMAX
color white, AARNA75352W_A_LNMAX
spectrum b, rainbow, AARNA75352W_A_LNMAX
set grid_slot, 3, AARNA75352W_A_LNMAX
center AARNA75352W_A_LNMAX
orient AARNA75352W_A_LNMAX
show surface, AARNA75352W_A_LNMAX
load AARNA75352W_A_LNMIN.pdb
hide everything, AARNA75352W_A_LNMIN
show cartoon, AARNA75352W_A_LNMIN
color white, AARNA75352W_A_LNMIN
spectrum b, rainbow, AARNA75352W_A_LNMIN
set grid_slot, 4, AARNA75352W_A_LNMIN
center AARNA75352W_A_LNMIN
orient AARNA75352W_A_LNMIN
show surface, AARNA75352W_A_LNMIN
set seq_view, 1
set transparency, 0.0
