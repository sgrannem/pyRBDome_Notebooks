set grid_mode,1
set surface_quality, 0
load AARNA6704317W_7_BP.pdb
hide everything, AARNA6704317W_7_BP
show cartoon, AARNA6704317W_7_BP
color white, AARNA6704317W_7_BP
spectrum b, rainbow, AARNA6704317W_7_BP
set grid_slot, 1, AARNA6704317W_7_BP
center AARNA6704317W_7_BP
orient AARNA6704317W_7_BP
show surface, AARNA6704317W_7_BP
load AARNA6704317W_7_EC.pdb
hide everything, AARNA6704317W_7_EC
show cartoon, AARNA6704317W_7_EC
color white, AARNA6704317W_7_EC
spectrum b, rainbow, AARNA6704317W_7_EC
set grid_slot, 2, AARNA6704317W_7_EC
center AARNA6704317W_7_EC
orient AARNA6704317W_7_EC
show surface, AARNA6704317W_7_EC
load AARNA6704317W_7_LNMAX.pdb
hide everything, AARNA6704317W_7_LNMAX
show cartoon, AARNA6704317W_7_LNMAX
color white, AARNA6704317W_7_LNMAX
spectrum b, rainbow, AARNA6704317W_7_LNMAX
set grid_slot, 3, AARNA6704317W_7_LNMAX
center AARNA6704317W_7_LNMAX
orient AARNA6704317W_7_LNMAX
show surface, AARNA6704317W_7_LNMAX
load AARNA6704317W_7_LNMIN.pdb
hide everything, AARNA6704317W_7_LNMIN
show cartoon, AARNA6704317W_7_LNMIN
color white, AARNA6704317W_7_LNMIN
spectrum b, rainbow, AARNA6704317W_7_LNMIN
set grid_slot, 4, AARNA6704317W_7_LNMIN
center AARNA6704317W_7_LNMIN
orient AARNA6704317W_7_LNMIN
show surface, AARNA6704317W_7_LNMIN
set seq_view, 1
set transparency, 0.0
