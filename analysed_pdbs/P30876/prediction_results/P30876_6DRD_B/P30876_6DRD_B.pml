set grid_mode,1
set surface_quality, 0
load AARNA1059646W_B_BP.pdb
hide everything, AARNA1059646W_B_BP
show cartoon, AARNA1059646W_B_BP
color white, AARNA1059646W_B_BP
spectrum b, rainbow, AARNA1059646W_B_BP
set grid_slot, 1, AARNA1059646W_B_BP
center AARNA1059646W_B_BP
orient AARNA1059646W_B_BP
show surface, AARNA1059646W_B_BP
load AARNA1059646W_B_EC.pdb
hide everything, AARNA1059646W_B_EC
show cartoon, AARNA1059646W_B_EC
color white, AARNA1059646W_B_EC
spectrum b, rainbow, AARNA1059646W_B_EC
set grid_slot, 2, AARNA1059646W_B_EC
center AARNA1059646W_B_EC
orient AARNA1059646W_B_EC
show surface, AARNA1059646W_B_EC
load AARNA1059646W_B_LNMAX.pdb
hide everything, AARNA1059646W_B_LNMAX
show cartoon, AARNA1059646W_B_LNMAX
color white, AARNA1059646W_B_LNMAX
spectrum b, rainbow, AARNA1059646W_B_LNMAX
set grid_slot, 3, AARNA1059646W_B_LNMAX
center AARNA1059646W_B_LNMAX
orient AARNA1059646W_B_LNMAX
show surface, AARNA1059646W_B_LNMAX
load AARNA1059646W_B_LNMIN.pdb
hide everything, AARNA1059646W_B_LNMIN
show cartoon, AARNA1059646W_B_LNMIN
color white, AARNA1059646W_B_LNMIN
spectrum b, rainbow, AARNA1059646W_B_LNMIN
set grid_slot, 4, AARNA1059646W_B_LNMIN
center AARNA1059646W_B_LNMIN
orient AARNA1059646W_B_LNMIN
show surface, AARNA1059646W_B_LNMIN
set seq_view, 1
set transparency, 0.0
