set grid_mode,1
set surface_quality, 0
load AARNA6236806W_W_BP.pdb
hide everything, AARNA6236806W_W_BP
show cartoon, AARNA6236806W_W_BP
color white, AARNA6236806W_W_BP
spectrum b, rainbow, AARNA6236806W_W_BP
set grid_slot, 1, AARNA6236806W_W_BP
center AARNA6236806W_W_BP
orient AARNA6236806W_W_BP
show surface, AARNA6236806W_W_BP
load AARNA6236806W_W_EC.pdb
hide everything, AARNA6236806W_W_EC
show cartoon, AARNA6236806W_W_EC
color white, AARNA6236806W_W_EC
spectrum b, rainbow, AARNA6236806W_W_EC
set grid_slot, 2, AARNA6236806W_W_EC
center AARNA6236806W_W_EC
orient AARNA6236806W_W_EC
show surface, AARNA6236806W_W_EC
load AARNA6236806W_W_LNMAX.pdb
hide everything, AARNA6236806W_W_LNMAX
show cartoon, AARNA6236806W_W_LNMAX
color white, AARNA6236806W_W_LNMAX
spectrum b, rainbow, AARNA6236806W_W_LNMAX
set grid_slot, 3, AARNA6236806W_W_LNMAX
center AARNA6236806W_W_LNMAX
orient AARNA6236806W_W_LNMAX
show surface, AARNA6236806W_W_LNMAX
load AARNA6236806W_W_LNMIN.pdb
hide everything, AARNA6236806W_W_LNMIN
show cartoon, AARNA6236806W_W_LNMIN
color white, AARNA6236806W_W_LNMIN
spectrum b, rainbow, AARNA6236806W_W_LNMIN
set grid_slot, 4, AARNA6236806W_W_LNMIN
center AARNA6236806W_W_LNMIN
orient AARNA6236806W_W_LNMIN
show surface, AARNA6236806W_W_LNMIN
set seq_view, 1
set transparency, 0.0
