set grid_mode,1
set surface_quality, 0
load AARNA7772604W_F_BP.pdb
hide everything, AARNA7772604W_F_BP
show cartoon, AARNA7772604W_F_BP
color white, AARNA7772604W_F_BP
spectrum b, rainbow, AARNA7772604W_F_BP
set grid_slot, 1, AARNA7772604W_F_BP
center AARNA7772604W_F_BP
orient AARNA7772604W_F_BP
show surface, AARNA7772604W_F_BP
load AARNA7772604W_F_EC.pdb
hide everything, AARNA7772604W_F_EC
show cartoon, AARNA7772604W_F_EC
color white, AARNA7772604W_F_EC
spectrum b, rainbow, AARNA7772604W_F_EC
set grid_slot, 2, AARNA7772604W_F_EC
center AARNA7772604W_F_EC
orient AARNA7772604W_F_EC
show surface, AARNA7772604W_F_EC
load AARNA7772604W_F_LNMAX.pdb
hide everything, AARNA7772604W_F_LNMAX
show cartoon, AARNA7772604W_F_LNMAX
color white, AARNA7772604W_F_LNMAX
spectrum b, rainbow, AARNA7772604W_F_LNMAX
set grid_slot, 3, AARNA7772604W_F_LNMAX
center AARNA7772604W_F_LNMAX
orient AARNA7772604W_F_LNMAX
show surface, AARNA7772604W_F_LNMAX
load AARNA7772604W_F_LNMIN.pdb
hide everything, AARNA7772604W_F_LNMIN
show cartoon, AARNA7772604W_F_LNMIN
color white, AARNA7772604W_F_LNMIN
spectrum b, rainbow, AARNA7772604W_F_LNMIN
set grid_slot, 4, AARNA7772604W_F_LNMIN
center AARNA7772604W_F_LNMIN
orient AARNA7772604W_F_LNMIN
show surface, AARNA7772604W_F_LNMIN
set seq_view, 1
set transparency, 0.0
