set grid_mode,1
set surface_quality, 0
load AARNA3147385W_L_BP.pdb
hide everything, AARNA3147385W_L_BP
show cartoon, AARNA3147385W_L_BP
color white, AARNA3147385W_L_BP
spectrum b, rainbow, AARNA3147385W_L_BP
set grid_slot, 1, AARNA3147385W_L_BP
center AARNA3147385W_L_BP
orient AARNA3147385W_L_BP
show surface, AARNA3147385W_L_BP
load AARNA3147385W_L_EC.pdb
hide everything, AARNA3147385W_L_EC
show cartoon, AARNA3147385W_L_EC
color white, AARNA3147385W_L_EC
spectrum b, rainbow, AARNA3147385W_L_EC
set grid_slot, 2, AARNA3147385W_L_EC
center AARNA3147385W_L_EC
orient AARNA3147385W_L_EC
show surface, AARNA3147385W_L_EC
load AARNA3147385W_L_LNMAX.pdb
hide everything, AARNA3147385W_L_LNMAX
show cartoon, AARNA3147385W_L_LNMAX
color white, AARNA3147385W_L_LNMAX
spectrum b, rainbow, AARNA3147385W_L_LNMAX
set grid_slot, 3, AARNA3147385W_L_LNMAX
center AARNA3147385W_L_LNMAX
orient AARNA3147385W_L_LNMAX
show surface, AARNA3147385W_L_LNMAX
load AARNA3147385W_L_LNMIN.pdb
hide everything, AARNA3147385W_L_LNMIN
show cartoon, AARNA3147385W_L_LNMIN
color white, AARNA3147385W_L_LNMIN
spectrum b, rainbow, AARNA3147385W_L_LNMIN
set grid_slot, 4, AARNA3147385W_L_LNMIN
center AARNA3147385W_L_LNMIN
orient AARNA3147385W_L_LNMIN
show surface, AARNA3147385W_L_LNMIN
set seq_view, 1
set transparency, 0.0
