set grid_mode,1
set surface_quality, 0
load AARNA358761W_A_BP.pdb
hide everything, AARNA358761W_A_BP
show cartoon, AARNA358761W_A_BP
color white, AARNA358761W_A_BP
spectrum b, rainbow, AARNA358761W_A_BP
set grid_slot, 1, AARNA358761W_A_BP
center AARNA358761W_A_BP
orient AARNA358761W_A_BP
show surface, AARNA358761W_A_BP
load AARNA358761W_A_EC.pdb
hide everything, AARNA358761W_A_EC
show cartoon, AARNA358761W_A_EC
color white, AARNA358761W_A_EC
spectrum b, rainbow, AARNA358761W_A_EC
set grid_slot, 2, AARNA358761W_A_EC
center AARNA358761W_A_EC
orient AARNA358761W_A_EC
show surface, AARNA358761W_A_EC
load AARNA358761W_A_LNMAX.pdb
hide everything, AARNA358761W_A_LNMAX
show cartoon, AARNA358761W_A_LNMAX
color white, AARNA358761W_A_LNMAX
spectrum b, rainbow, AARNA358761W_A_LNMAX
set grid_slot, 3, AARNA358761W_A_LNMAX
center AARNA358761W_A_LNMAX
orient AARNA358761W_A_LNMAX
show surface, AARNA358761W_A_LNMAX
load AARNA358761W_A_LNMIN.pdb
hide everything, AARNA358761W_A_LNMIN
show cartoon, AARNA358761W_A_LNMIN
color white, AARNA358761W_A_LNMIN
spectrum b, rainbow, AARNA358761W_A_LNMIN
set grid_slot, 4, AARNA358761W_A_LNMIN
center AARNA358761W_A_LNMIN
orient AARNA358761W_A_LNMIN
show surface, AARNA358761W_A_LNMIN
set seq_view, 1
set transparency, 0.0
