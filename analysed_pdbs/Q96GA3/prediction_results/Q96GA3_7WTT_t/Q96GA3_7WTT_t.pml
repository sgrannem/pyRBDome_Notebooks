set grid_mode,1
set surface_quality, 0
load AARNA4336322W_t_BP.pdb
hide everything, AARNA4336322W_t_BP
show cartoon, AARNA4336322W_t_BP
color white, AARNA4336322W_t_BP
spectrum b, rainbow, AARNA4336322W_t_BP
set grid_slot, 1, AARNA4336322W_t_BP
center AARNA4336322W_t_BP
orient AARNA4336322W_t_BP
show surface, AARNA4336322W_t_BP
load AARNA4336322W_t_EC.pdb
hide everything, AARNA4336322W_t_EC
show cartoon, AARNA4336322W_t_EC
color white, AARNA4336322W_t_EC
spectrum b, rainbow, AARNA4336322W_t_EC
set grid_slot, 2, AARNA4336322W_t_EC
center AARNA4336322W_t_EC
orient AARNA4336322W_t_EC
show surface, AARNA4336322W_t_EC
load AARNA4336322W_t_LNMAX.pdb
hide everything, AARNA4336322W_t_LNMAX
show cartoon, AARNA4336322W_t_LNMAX
color white, AARNA4336322W_t_LNMAX
spectrum b, rainbow, AARNA4336322W_t_LNMAX
set grid_slot, 3, AARNA4336322W_t_LNMAX
center AARNA4336322W_t_LNMAX
orient AARNA4336322W_t_LNMAX
show surface, AARNA4336322W_t_LNMAX
load AARNA4336322W_t_LNMIN.pdb
hide everything, AARNA4336322W_t_LNMIN
show cartoon, AARNA4336322W_t_LNMIN
color white, AARNA4336322W_t_LNMIN
spectrum b, rainbow, AARNA4336322W_t_LNMIN
set grid_slot, 4, AARNA4336322W_t_LNMIN
center AARNA4336322W_t_LNMIN
orient AARNA4336322W_t_LNMIN
show surface, AARNA4336322W_t_LNMIN
set seq_view, 1
set transparency, 0.0
