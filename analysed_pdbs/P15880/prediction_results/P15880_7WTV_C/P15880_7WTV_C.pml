set grid_mode,1
set surface_quality, 0
load AARNA453758W_C_BP.pdb
hide everything, AARNA453758W_C_BP
show cartoon, AARNA453758W_C_BP
color white, AARNA453758W_C_BP
spectrum b, rainbow, AARNA453758W_C_BP
set grid_slot, 1, AARNA453758W_C_BP
center AARNA453758W_C_BP
orient AARNA453758W_C_BP
show surface, AARNA453758W_C_BP
load AARNA453758W_C_EC.pdb
hide everything, AARNA453758W_C_EC
show cartoon, AARNA453758W_C_EC
color white, AARNA453758W_C_EC
spectrum b, rainbow, AARNA453758W_C_EC
set grid_slot, 2, AARNA453758W_C_EC
center AARNA453758W_C_EC
orient AARNA453758W_C_EC
show surface, AARNA453758W_C_EC
load AARNA453758W_C_LNMAX.pdb
hide everything, AARNA453758W_C_LNMAX
show cartoon, AARNA453758W_C_LNMAX
color white, AARNA453758W_C_LNMAX
spectrum b, rainbow, AARNA453758W_C_LNMAX
set grid_slot, 3, AARNA453758W_C_LNMAX
center AARNA453758W_C_LNMAX
orient AARNA453758W_C_LNMAX
show surface, AARNA453758W_C_LNMAX
load AARNA453758W_C_LNMIN.pdb
hide everything, AARNA453758W_C_LNMIN
show cartoon, AARNA453758W_C_LNMIN
color white, AARNA453758W_C_LNMIN
spectrum b, rainbow, AARNA453758W_C_LNMIN
set grid_slot, 4, AARNA453758W_C_LNMIN
center AARNA453758W_C_LNMIN
orient AARNA453758W_C_LNMIN
show surface, AARNA453758W_C_LNMIN
set seq_view, 1
set transparency, 0.0
