set grid_mode,1
set surface_quality, 0
load AARNA6278264W_T_BP.pdb
hide everything, AARNA6278264W_T_BP
show cartoon, AARNA6278264W_T_BP
color white, AARNA6278264W_T_BP
spectrum b, rainbow, AARNA6278264W_T_BP
set grid_slot, 1, AARNA6278264W_T_BP
center AARNA6278264W_T_BP
orient AARNA6278264W_T_BP
show surface, AARNA6278264W_T_BP
load AARNA6278264W_T_EC.pdb
hide everything, AARNA6278264W_T_EC
show cartoon, AARNA6278264W_T_EC
color white, AARNA6278264W_T_EC
spectrum b, rainbow, AARNA6278264W_T_EC
set grid_slot, 2, AARNA6278264W_T_EC
center AARNA6278264W_T_EC
orient AARNA6278264W_T_EC
show surface, AARNA6278264W_T_EC
load AARNA6278264W_T_LNMAX.pdb
hide everything, AARNA6278264W_T_LNMAX
show cartoon, AARNA6278264W_T_LNMAX
color white, AARNA6278264W_T_LNMAX
spectrum b, rainbow, AARNA6278264W_T_LNMAX
set grid_slot, 3, AARNA6278264W_T_LNMAX
center AARNA6278264W_T_LNMAX
orient AARNA6278264W_T_LNMAX
show surface, AARNA6278264W_T_LNMAX
load AARNA6278264W_T_LNMIN.pdb
hide everything, AARNA6278264W_T_LNMIN
show cartoon, AARNA6278264W_T_LNMIN
color white, AARNA6278264W_T_LNMIN
spectrum b, rainbow, AARNA6278264W_T_LNMIN
set grid_slot, 4, AARNA6278264W_T_LNMIN
center AARNA6278264W_T_LNMIN
orient AARNA6278264W_T_LNMIN
show surface, AARNA6278264W_T_LNMIN
set seq_view, 1
set transparency, 0.0
