set grid_mode,1
set surface_quality, 0
load AARNA6911062W_X_BP.pdb
hide everything, AARNA6911062W_X_BP
show cartoon, AARNA6911062W_X_BP
color white, AARNA6911062W_X_BP
spectrum b, rainbow, AARNA6911062W_X_BP
set grid_slot, 1, AARNA6911062W_X_BP
center AARNA6911062W_X_BP
orient AARNA6911062W_X_BP
show surface, AARNA6911062W_X_BP
load AARNA6911062W_X_EC.pdb
hide everything, AARNA6911062W_X_EC
show cartoon, AARNA6911062W_X_EC
color white, AARNA6911062W_X_EC
spectrum b, rainbow, AARNA6911062W_X_EC
set grid_slot, 2, AARNA6911062W_X_EC
center AARNA6911062W_X_EC
orient AARNA6911062W_X_EC
show surface, AARNA6911062W_X_EC
load AARNA6911062W_X_LNMAX.pdb
hide everything, AARNA6911062W_X_LNMAX
show cartoon, AARNA6911062W_X_LNMAX
color white, AARNA6911062W_X_LNMAX
spectrum b, rainbow, AARNA6911062W_X_LNMAX
set grid_slot, 3, AARNA6911062W_X_LNMAX
center AARNA6911062W_X_LNMAX
orient AARNA6911062W_X_LNMAX
show surface, AARNA6911062W_X_LNMAX
load AARNA6911062W_X_LNMIN.pdb
hide everything, AARNA6911062W_X_LNMIN
show cartoon, AARNA6911062W_X_LNMIN
color white, AARNA6911062W_X_LNMIN
spectrum b, rainbow, AARNA6911062W_X_LNMIN
set grid_slot, 4, AARNA6911062W_X_LNMIN
center AARNA6911062W_X_LNMIN
orient AARNA6911062W_X_LNMIN
show surface, AARNA6911062W_X_LNMIN
set seq_view, 1
set transparency, 0.0
