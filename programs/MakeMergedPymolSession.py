import pymol
import os
import sys
import glob
import argparse
from pymol import *

parser = argparse.ArgumentParser(description="Script for merging domain analysis results from pyRBDome data")
parser.add_argument("--domain_name",
                    dest="domain_name",
                    help="Name of domain that you want to generate the pymol session for.",
                    metavar="RRM_1",
                    default=None)
parser.add_argument("--pdb_dir",
                    dest="pdb_dir",
                    help="The location where the aligned pdb files are stored.",
                    default=None)
parser.add_argument("--outdirname",
                    dest="outdirname",
                    help="Name of the directory where you want to store the final pymol session.",
                    metavar="MMAlign_results",
                    default="./")
parser.add_argument("--overwrite",
                    dest="overwrite",
                    help="To overwrite any existing output files. Default is False",
                    action="store_true")

### Parsing the arguments:
args = parser.parse_args()

### Defining the directory where the new pdb files will be stored:
if not os.path.exists(args.outdirname):
    os.makedirs(args.outdirname)

out_file_path = f"{args.outdirname}/{args.domain_name}_session.pse"

if os.path.exists and not args.overwrite:
    sys.stdout.write(f"The pymol session {args.domain_name}_session.pse already exists. Not overwriting!\n")
else:

    # Initialize PyMOL
    pymol.finish_launching()

    # Load PDB files containing the aligned domains:
    aligned_domains = list(glob.glob(f"{args.pdb_dir}/*_super.pdb"))
    if aligned_domains:

        # Create a set to store the loaded objects for this section
        domain_objects = set()
        
        # Load PDB files and select all atoms from each structure
        for pdb_file in aligned_domains:
            cmd.load(pdb_file)
            domain_objects.update(cmd.get_object_list())
            
        loaded_objects = domain_objects
        
        cmd.group("Aligned_domains",members=' '.join(loaded_objects))

        # Show surface representation for the group:
        cmd.show("surface", "Aligned_domains")
        cmd.hide("cartoon", "Aligned_domains")
        
        # Set the color of the surface to grayscale:
        cmd.color("gray", "Aligned_domains")
        
        # Set the transparency of the surface to 70%:
        # cmd.set("transparency", 70, "Aligned_domains")

    # Load PDB files containing the cross-linked peptides
    aligned_peptides = list(glob.glob(f"{args.pdb_dir}/*_peptides.pdb"))
    if aligned_peptides:

        # Create a set to store the loaded objects for this section
        peptide_objects = set()
    
        # Load PDB files and select all atoms from each structure
        for pdb_file in aligned_peptides:
            cmd.load(pdb_file)
            peptide_objects.update(cmd.get_object_list())
        
        loaded_objects = peptide_objects - domain_objects
        
        cmd.group("Aligned_peptides",members=' '.join(loaded_objects))

        # Show cartoon representation
        cmd.show("cartoon", "Aligned_peptides")
        
        # Set the color of the surface to grayscale:
        cmd.color("blue", "Aligned_peptides")
    
    # Load PDB files containing the cross-linked amino acids:
    aligned_amino_acids = list(glob.glob(f"{args.pdb_dir}/*_amino_acids.pdb"))
    if aligned_amino_acids:
    
        # Create a set to store the loaded objects for this section
        amino_acid_objects = set()
    
        # Create a group:
        group_name = "Aligned_amino_acids"
    
        # Load PDB files and select all atoms from each structure
        for pdb_file in aligned_amino_acids:
            cmd.load(pdb_file)
            amino_acid_objects.update(cmd.get_object_list())
            
        loaded_objects = amino_acid_objects - peptide_objects
                
        cmd.group("Aligned_amino_acids",members=' '.join(loaded_objects))
            
        # Show sticks representation
        cmd.show("sticks", "Aligned_amino_acids")
        
        # Set the color of the surface to grayscale:
        cmd.color("yellow", "Aligned_amino_acids")

    # Load PDB files containing the RNA-binding residues predicted by aaRNA:
    aligned_aaRNA_sites = list(glob.glob(f"{args.pdb_dir}/*_aaRNA_sites.pdb"))

    if aligned_aaRNA_sites:
        # Create a set to store the loaded objects for this section
        aarna_objects = set()
        
        # Create a group:
        group_name = "Aligned_aaRNA_sites"
    
        # Load PDB files and select all atoms from each structure
        for pdb_file in aligned_aaRNA_sites:
            cmd.load(pdb_file)
            aarna_objects.update(cmd.get_object_list())
            
        loaded_objects = aarna_objects - amino_acid_objects
                
        cmd.group("Aligned_aaRNA_sites",members=' '.join(loaded_objects))
            
        # Show sticks representation
        cmd.show("sticks", "Aligned_aaRNA_sites")
        
        # Set the color of the surface to grayscale:
        cmd.color("red", "Aligned_aaRNA_sites")
    
    # Clear the set of loaded objects to prepare for the next section
    loaded_objects.clear()
    
    # Save a session or display the viewer
    cmd.save(out_file_path)  # Save a session for later use
    cmd.viewport(800, 600)   # Set the viewer size
    
    # Close PyMOL
    cmd.quit()

