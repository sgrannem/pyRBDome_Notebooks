
Name of Chain_1: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/RRM_1/Q15056_AF_A_RRM_1_1.pdb:A (to be superimposed onto Chain_2)
Name of Chain_2: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/RRM_1/A0AV96_AF_A_RRM_1_1.pdb:A
Length of Chain_1: 67 residues
Length of Chain_2: 65 residues

Aligned length= 62, RMSD=   1.32, Seq_ID=n_identical/n_aligned= 0.210
TM-score= 0.78366 (if normalized by length of Chain_1, i.e., LN=67, d0=2.83)
TM-score= 0.80321 (if normalized by length of Chain_2, i.e., LN=65, d0=2.77)
(You should use TM-score normalized by length of the reference structure)

(":" denotes residue pairs of d <  5.0 Angstrom, "." denotes other aligned residues)
-YVGNLPFNTVQGDIDAIFKD-LSIRSVRLVRDKDTDKFKGFCYVEFDEVDSLKEALT-YDGALLGDRSL
 :::::::::::::::::::: :::::::::::: ::::::::::::::::::::::: :::::::    
VFVGKIPRDVYEDELVPVFEAVGRIYELRLMMDF-DGKNRGYAFVMYCHKHEAKRAVRELNNYEIR----

Total CPU time is  0.01 seconds
