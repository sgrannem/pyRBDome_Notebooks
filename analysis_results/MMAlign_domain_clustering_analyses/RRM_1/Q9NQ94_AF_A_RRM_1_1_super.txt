
Name of Chain_1: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/RRM_1/Q9NQ94_AF_A_RRM_1_1.pdb:A (to be superimposed onto Chain_2)
Name of Chain_2: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/RRM_1/A0AV96_AF_A_RRM_1_1.pdb:A
Length of Chain_1: 67 residues
Length of Chain_2: 65 residues

Aligned length= 65, RMSD=   0.37, Seq_ID=n_identical/n_aligned= 0.631
TM-score= 0.95393 (if normalized by length of Chain_1, i.e., LN=67, d0=2.83)
TM-score= 0.98258 (if normalized by length of Chain_2, i.e., LN=65, d0=2.77)
(You should use TM-score normalized by length of the reference structure)

(":" denotes residue pairs of d <  5.0 Angstrom, "." denotes other aligned residues)
IFIGKLPRDLFEDELIPLCEKIGKIYEMRMMMDFNGNNRGYAFVTFSNKVEAKNAIKQLNNYEIRNG
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  
VFVGKIPRDVYEDELVPVFEAVGRIYELRLMMDFDGKNRGYAFVMYCHKHEAKRAVRELNNYEIR--

Total CPU time is  0.01 seconds
