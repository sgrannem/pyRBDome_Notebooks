
Name of Chain_1: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/RRM_1/P31943_6DHS_A_RRM_1_1.pdb:A (to be superimposed onto Chain_2)
Name of Chain_2: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/RRM_1/A0AV96_AF_A_RRM_1_1.pdb:A
Length of Chain_1: 51 residues
Length of Chain_2: 65 residues

Aligned length= 46, RMSD=   1.58, Seq_ID=n_identical/n_aligned= 0.283
TM-score= 0.67336 (if normalized by length of Chain_1, i.e., LN=51, d0=2.29)
TM-score= 0.56526 (if normalized by length of Chain_2, i.e., LN=65, d0=2.77)
(You should use TM-score normalized by length of the reference structure)

(":" denotes residue pairs of d <  5.0 Angstrom, "." denotes other aligned residues)
VVKVRGLPWSCSADEVQRFF-SDCKIQNGAQGIRFIYTREGRPSGEAFVELE------------------
 ::::::::::::::::::: ::::::    :::::::::::::::::::::                  
-VFVGKIPRDVYEDELVPVFEAVGRIY----ELRLMMDFDGKNRGYAFVMYCHKHEAKRAVRELNNYEIR

Total CPU time is  0.00 seconds
