
Name of Chain_1: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/RRM_1/Q15427_7QTT_F_RRM_1_1.pdb:F (to be superimposed onto Chain_2)
Name of Chain_2: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/RRM_1/A0AV96_AF_A_RRM_1_1.pdb:A
Length of Chain_1: 63 residues
Length of Chain_2: 65 residues

Aligned length= 59, RMSD=   1.59, Seq_ID=n_identical/n_aligned= 0.271
TM-score= 0.73891 (if normalized by length of Chain_1, i.e., LN=63, d0=2.71)
TM-score= 0.72174 (if normalized by length of Chain_2, i.e., LN=65, d0=2.77)
(You should use TM-score normalized by length of the reference structure)

(":" denotes residue pairs of d <  5.0 Angstrom, "." denotes other aligned residues)
DATVYVGGLDEKVSEPLLWELFLQAGPVVNTHMPKDRVTGQHQGYGFVEFLSEEDADYAIKIM------
   :::::::::::::::::::::::::::::::::: :::::::::::::::::::::::::      
---VFVGKIPRDVYEDELVPVFEAVGRIYELRLMMDF-DGKNRGYAFVMYCHKHEAKRAVRELNNYEIR

Total CPU time is  0.00 seconds
