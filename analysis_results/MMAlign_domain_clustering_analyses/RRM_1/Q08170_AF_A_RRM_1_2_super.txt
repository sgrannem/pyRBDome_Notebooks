
Name of Chain_1: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/RRM_1/Q08170_AF_A_RRM_1_2.pdb:A (to be superimposed onto Chain_2)
Name of Chain_2: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/RRM_1/A0AV96_AF_A_RRM_1_1.pdb:A
Length of Chain_1: 66 residues
Length of Chain_2: 65 residues

Aligned length= 61, RMSD=   1.71, Seq_ID=n_identical/n_aligned= 0.131
TM-score= 0.78458 (if normalized by length of Chain_1, i.e., LN=66, d0=2.80)
TM-score= 0.79476 (if normalized by length of Chain_2, i.e., LN=65, d0=2.77)
(You should use TM-score normalized by length of the reference structure)

(":" denotes residue pairs of d <  5.0 Angstrom, "." denotes other aligned residues)
LIVENLSSRCSWQDLKDYMRQAGEVTYADAHKG----RKNEGVIEFVSYSDMKRALEKLDGTEVNGRKIR
::::::::::::::::::::::::::::::::.    .:::::::::::::::::::::::::: :    
VFVGKIPRDVYEDELVPVFEAVGRIYELRLMMDFDGKNRGYAFVMYCHKHEAKRAVRELNNYEI-R----

Total CPU time is  0.01 seconds
