
Name of Chain_1: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/RRM_1/Q13310_AF_A_RRM_1_1.pdb:A (to be superimposed onto Chain_2)
Name of Chain_2: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/RRM_1/A0AV96_AF_A_RRM_1_1.pdb:A
Length of Chain_1: 71 residues
Length of Chain_2: 65 residues

Aligned length= 65, RMSD=   0.93, Seq_ID=n_identical/n_aligned= 0.292
TM-score= 0.84008 (if normalized by length of Chain_1, i.e., LN=71, d0=2.94)
TM-score= 0.90885 (if normalized by length of Chain_2, i.e., LN=65, d0=2.77)
(You should use TM-score normalized by length of the reference structure)

(":" denotes residue pairs of d <  5.0 Angstrom, "." denotes other aligned residues)
LYVGDLHSDVTEAMLYEKFSPAGPVLSIRVCRDMITRRSLGYAYVNFQQPADAERALDTMNFDVIKGKPIR
::::::::::::::::::::::::::::::::: ::::::::::::::::::::::::::::::::     
VFVGKIPRDVYEDELVPVFEAVGRIYELRLMMD-FDGKNRGYAFVMYCHKHEAKRAVRELNNYEIR-----

Total CPU time is  0.00 seconds
