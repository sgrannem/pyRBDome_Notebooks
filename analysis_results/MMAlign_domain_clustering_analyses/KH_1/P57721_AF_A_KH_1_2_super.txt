
Name of Chain_1: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/KH_1/P57721_AF_A_KH_1_2.pdb:A (to be superimposed onto Chain_2)
Name of Chain_2: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/KH_1/A1L020_AF_A_KH_1_1.pdb:A
Length of Chain_1: 64 residues
Length of Chain_2: 61 residues

Aligned length= 59, RMSD=   1.38, Seq_ID=n_identical/n_aligned= 0.186
TM-score= 0.79253 (if normalized by length of Chain_1, i.e., LN=64, d0=2.74)
TM-score= 0.82526 (if normalized by length of Chain_2, i.e., LN=61, d0=2.64)
(You should use TM-score normalized by length of the reference structure)

(":" denotes residue pairs of d <  5.0 Angstrom, "." denotes other aligned residues)
TLRLVV-PASQCGSLIGKGGSKIKEIRESTGAQVQVAGDMLPNSTERAVTISGTPDAIIQCVKQI-
 ::::: ::::::::::::::::::::::::::::::::::    :::::::::::::::::::: 
-ECVPVPTSEHVAEIVGRQGCKIKALRAKTNTYIKTPVRGE----EPVFMVTGRREDVATARREII

Total CPU time is  0.01 seconds
