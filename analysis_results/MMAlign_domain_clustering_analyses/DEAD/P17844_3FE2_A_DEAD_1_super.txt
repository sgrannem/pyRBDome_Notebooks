
Name of Chain_1: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/DEAD/P17844_3FE2_A_DEAD_1.pdb:A (to be superimposed onto Chain_2)
Name of Chain_2: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/DEAD/O00148_AF_A_DEAD_1.pdb:A
Length of Chain_1: 148 residues
Length of Chain_2: 166 residues

Aligned length= 95, RMSD=   1.38, Seq_ID=n_identical/n_aligned= 0.253
TM-score= 0.59503 (if normalized by length of Chain_1, i.e., LN=148, d0=4.53)
TM-score= 0.53429 (if normalized by length of Chain_2, i.e., LN=166, d0=4.80)
(You should use TM-score normalized by length of the reference structure)

(":" denotes residue pairs of d <  5.0 Angstrom, "." denotes other aligned residues)
QEVETYRRSKEITVRGHNCPKPVLNFYEANFPANVMDVIARQNFTEPTAIQAQGWPVALSGLDMVGVAQTGSGKTLSYLLPAIVHINHQPFLERGDGPICLVLAPTRELAQQVQQVAAEYCRACR-LKSTCIYGGAPKGPQIRDLE-RGV---------------------------------------------------------------------
                                                :::::::::::::::::::::::::::::::::::::::   :::: : :::::::::::::::::::::::::::: :::::::::::::::::::: :::                                                                     
------------------------------------------------EVQHECIPQAILGMDVLCQAKSGMGKTAVFVLATLQQIE---PVNG-Q-VTVLVMCHTRELAFQISKEYERFSKYMPSVKVSVFFGGLSIKKDEEVLKKNCPHVVVGTPGRILALVRNRSFSLKNVKHFVLDECDKMLEQLDMRRDVQEIFRLTPHEKQCMMFSATLSKDI

Total CPU time is  0.04 seconds
