
Name of Chain_1: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/CSD/O75534_AF_A_CSD_1.pdb:A (to be superimposed onto Chain_2)
Name of Chain_2: /localdisk/home/openngs/openngs/notebooks/ngs/sander/pyrbdome_full_160823/analysis_results/MMAlign_domain_clustering_analyses/CSD/O75534_AF_A_CSD_1.pdb:A
Length of Chain_1: 63 residues
Length of Chain_2: 63 residues

Aligned length= 63, RMSD=   0.00, Seq_ID=n_identical/n_aligned= 1.000
TM-score= 1.00000 (if normalized by length of Chain_1, i.e., LN=63, d0=2.71)
TM-score= 1.00000 (if normalized by length of Chain_2, i.e., LN=63, d0=2.71)
(You should use TM-score normalized by length of the reference structure)

(":" denotes residue pairs of d <  5.0 Angstrom, "." denotes other aligned residues)
TGVIEKLLTSYGFIQCSERQARLFFHCSQYNGNLQDLKVGDDVEFEVSSDRRTGKPIAVKLVK
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
TGVIEKLLTSYGFIQCSERQARLFFHCSQYNGNLQDLKVGDDVEFEVSSDRRTGKPIAVKLVK

Total CPU time is  0.01 seconds
